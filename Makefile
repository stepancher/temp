ifdef ${.CURDIR}
	CURRENT_DIR = ${.CURDIR}
else
	CURRENT_DIR = ${CURDIR}
endif

all:
	@echo 'you must enter target: build-prod, build-test, build-dev, build-local or update-prod, update-test, update-dev, update-local'

build-prod: install-vendor check-chooser chooser prod yii-migrate localization-parse upload-777 compress_assets
build-test: install-vendor check-chooser chooser prod yii-migrate localization-parse upload-777 compress_assets
build-dev: install-vendor-dev check-chooser chooser dev yii-migrate localization-parse upload-777
build-local: install-vendor-dev check-chooser chooser dev yii-migrate localization-parse upload-777

update-prod: install-vendor check-chooser chooser prod yii-migrate-update localization-parse compress_assets
update-test: install-vendor check-chooser chooser test yii-migrate-update localization-parse compress_assets
update-dev: install-vendor-dev check-chooser chooser dev yii-migrate-update localization-parse
update-local: install-vendor-dev check-chooser chooser dev yii-migrate-update localization-parse

localization-parse:
	php yii localization/parse

upload-777:
	chmod -R 777 ${CURRENT_DIR}/upload

compress_assets:
	php ${CURRENT_DIR}/yii asset ${CURRENT_DIR}/backend/config/assets.php ${CURRENT_DIR}/backend/config/assets-min.php

plug-dev:
	php ${CURRENT_DIR}/vendor/quartz/yii2-utilities/plug-site.php -s "$(plug-status)" -i "$(plug-interval)" -e "$(plug-end)"

plug-prod: plug-dev clear_cache_prod

plug-open:  #открыть сайт из консоли
	php ${CURRENT_DIR}/vendor/quartz/yii2-utilities/plug-site.php -s "false"

plug-close: #закрыть сайт из консоли по быстрому, время будет указанное ранее
	php ${CURRENT_DIR}/vendor/quartz/yii2-utilities/plug-site.php -s "true"

clear_cache_prod:
	php ${CURRENT_DIR}/vendor/quartz/yii2-utilities/cloudflare-api.php -c="clear_cache" --path="${CURRENT_DIR}/common/config/cloudflare-config.json"

check-chooser:
ifneq ($(use-check-chooser), false)
	php ./vendor/s.yanchuk/env-chooser/check-chooser.php --dir=${CURRENT_DIR}
endif

chooser:
	php ./vendor/s.yanchuk/env-chooser/env-chooser.php --env=default --dir=${CURRENT_DIR}

install-vendor-dev:
	composer global require "fxp/composer-asset-plugin:1.2.0"
	composer install

install-vendor:
	composer global require "fxp/composer-asset-plugin:1.2.0"
	composer install

prod:
	php ${CURRENT_DIR}/init --env=Production --overwrite=All

dev:
	php ${CURRENT_DIR}/init --env=Development --overwrite=All

test:
	php ${CURRENT_DIR}/init --env=Test --overwrite=All

yii-migrate:
	php yii migrate --migrationPath=@vendor/quartz/yii2-rbac/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-user/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-menu/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-metatag/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-pages/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-rollback/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-settings-module/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-blocks/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-forms/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/mailnotify/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/kartik-v/yii2-dynagrid/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-localization/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-servertest/migrations --interactive=0
	php yii migrate --migrationPath=@vendor/quartz/yii2-adminlte-theme/migrations --interactive=0
	php yii migrate --migrationPath=vendor/quartz/yii2-reference/migrations --interactive=0
	php yii migrate --interactive=0

yii-migrate-update:
	php yii migrate --interactive=0
