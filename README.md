Шаблоное приложение для приложений, базируется на модулях quartz
===============================

Установка

1) git clone git@gitlab.freematiq.com:quartz/yii2-base-app.git

2) в корне лежит chooser.json.distr копируем его без distr прописываем все конфиги

3) Запускаем make build-dev(prod) проект установлен

4) В последствии использовать make update-dev(prod)

-------------------

Генерация документации
vendor/bin/apidoc api api/controllers ./documentation --template=freematiq 

-------------------
