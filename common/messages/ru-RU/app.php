<?php
return [
    '{attribute} cannot be blank.'=>'Заполните поле {attribute}',
    'Days Before' => '{n, plural, =0{дата истекла} one{# день} few{# дня} many{# дней} other{# дня}}',
    'Count Peoples' => '{n, plural, =0{человек} one{# человек} few{# человека} many{# человек} other{# человека}}',
    'Count Reviews' => '{n, plural, =0{нет отзывов} one{# отзыв} few{# отзыва} many{# отзывов} other{# отзыва}}',
    'Validation Error' => 'Ошибка валидации',
    'Missing post data' => 'Отсутствуют Post данные',
    'Authorization Error' => 'Ошибка авторизации',
    'You have successfully logged out' => 'Вы успешно вышли из системы',
    'ERROR_CAN_NOT_UPLOAD_FILE' => 'Невозможно загрузить файл',
    'The image is saved successfully' => 'Изображение успешно сохранено',
    '' => '',
];