<?php

return [
    'Name' =>'Код меню',
    'Subname'=>'Название подменю',
    'Create'=>'Создать',
    'Save'=>'Сохранить',
    'Before add submenu you need to save it'=>'Чтобы добавлять пункты в меню нажмите кнопку "создать"',
    'Menu have been saved successfully'=>'Меню сохранено',
    'Submenu have been saved successfully'=>'Пункт меню сохранен',
    'Submenu have been added'=>'Пункт меню добавлен',
    '{attribute} must be contain only latin characters'=>'Поле "{attribute}" должно содеражть только буквы латинского алфавита'
];

