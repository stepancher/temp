<?php
return [
    '--Select a category--'=>'--Выберите категорию--',
    'pid'=>'Родительская страница',
    'url'=>'URL страницы',
    'title'=>'Заголовок(тайтл)',
    'keywords'=>'Ключевые слова',
    'content'=>'Описание',
    'name'=>'Название',
    'description'=>'Содержание',
    'date'=>'Дата публикации',
    'enabled'=>'Отображать на сайте',
    
    'SEO configurations'=>'SEO настройки',
    'Basic configurations'=>'Основные настройки',
    
    'URL must be contain only latin characters numbers and simbols _ -'=>'URL должен содеражть только буквы латинского алфавита, цифры и символы _ -',
    'Save'=>'Сохранить',
    
    'Page have been saved successfully'=>'Страница успешно сохранена'
];