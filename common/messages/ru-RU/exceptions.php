<?php
return [
    'Oops something went wrong' => 'Упс что-то пошло не так',
    'error.activemq' => 'Ошибка очереди',
    'error.access' => 'Недостаточно прав',
    'error.validation' => 'Ошибка валидации',
    'error.authorization' => 'Ошибка авторизации',
    'error.not.content' => 'Контент не найден',
    '' => '',
];