<?php
return [
    'Your name' => 'Ваше имя',
    'Email address' => 'Электронная почта',
    'Project description' => 'Описание проекта',
    'Created At' => 'Дата заказа',
    'Order successfully added.' => 'Заказ успешно добавлен.',
    'The order is not added.' => 'Заказ не добавлен.',
    '"{attribute}" cannot be blank.' => 'Нужно заполнить "{attribute}"',
    'Doesn\'t look like email' => 'Не похоже на email адрес',
    'Doesn\'t look like a project description' => 'Не похоже на описание проекта',
    'Doesn\'t look like a name' => 'Не похоже на имя',
];