<?php
return [
	'yes' => 'Yes',
	'no' => 'No',
	'user.age' => 'There {n, plural, =0{are no cats} =1{is # cat} other{are # cats}}!',
];