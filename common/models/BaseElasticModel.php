<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 17.09.16
 * Time: 18:59
 */

namespace common\models;

use yii\elasticsearch\ActiveRecord;

abstract class BaseElasticModel extends ActiveRecord
{
    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * Create this model's index
     */
    public static function createIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->createIndex(static::index(), [
            'settings' => [ 'index' => ['refresh_interval' => '10s']],
            'mappings' => static::mapping(),
            //'aliases' => [ /* ... */ ],
            //'creation_date' => '...'
        ]);
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index(), static::type());
    }

    public static function updateRecord($model_id, $columns){
        try{
            $record = self::get($model_id);
            foreach($columns as $key => $value){
                $record->$key = $value;
            }
            return $record->update();
        }
        catch(\Exception $e){
            //handle error here
            return false;
        }
    }

    public static function deleteRecord($model_id)
    {
        try{
            $record = self::get($model_id);
            $record->delete();
            return 1;
        }
        catch(\Exception $e){
            //handle error here
            return false;
        }
    }

}