<?php

namespace common\models\complaint;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use quartz\tools\behaviors\AllToListBehavior;

/**
 * This is the model class for table "complaint".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $name
 * @property string $email
 * @property string $page
 * @property string $id_complaint_type
 * @property string $details
 * @property boolean $is_answered
 * @property string $answer
 * @property string $created_at
 * @property string $updated_at
 */
class Complaint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complaint';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'id_complaint_type', 'details'], 'required'],
            [['id_user'], 'integer'],
            [['page', 'id_complaint_type', 'details', 'answer'], 'string'],
            [['is_answered'], 'boolean'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => [],
            'frontend' => [
                'name', 'email', 'id_complaint_type', 'details'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('complaint', 'Id'),
            'id_user' => Yii::t('complaint', 'User'),
            'name' => Yii::t('complaint', 'Name'),
            'email' => Yii::t('complaint', 'Email'),
            'page' => Yii::t('complaint', 'Page with complaint'),
            'id_complaint_type' => Yii::t('complaint', 'Complaint type'),
            'details' => Yii::t('complaint', 'Details'),
            'is_answered' => Yii::t('complaint', 'Is complaint answered'),
            'answer' => Yii::t('complaint', 'Answer on complaint'),
            'created_at' => Yii::t('complaint', 'Created At'),
            'updated_at' => Yii::t('complaint', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return ComplaintQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ComplaintQuery(get_called_class());
    }

    /**
     * @inheritdoc
     * @return bool
     */
    public function beforeValidate()
    {
        if (!Yii::$app->user->isGuest) {
            $this->id_user = Yii::$app->user->getId();
        }

        return parent::beforeValidate();
    }
}
