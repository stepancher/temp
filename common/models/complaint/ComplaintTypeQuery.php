<?php

namespace common\models\complaint;

/**
 * This is the ActiveQuery class for [[ComplaintType]].
 *
 * @see ComplaintType
 */
class ComplaintTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
    return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ComplaintType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ComplaintType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
