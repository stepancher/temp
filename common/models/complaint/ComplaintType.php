<?php

namespace common\models\complaint;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\db\Expression;
use quartz\tools\behaviors\AllToListBehavior;

/**
 * This is the model class for table "complaint_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @mixin AllToListBehavior
 */
class ComplaintType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complaint_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['name', 'text'], 'string', 'max' => 255],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('complaint', 'Id'),
            'name' => Yii::t('complaint', 'Name'),
            'text' => Yii::t('complaint', 'Text'),
        ];
    }

    /**
     * @inheritdoc
     * @return ComplaintTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ComplaintTypeQuery(get_called_class());
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        TagDependency::invalidate(Yii::$app->cache, 'complaint_type');
        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        TagDependency::invalidate(Yii::$app->cache, 'complaint_type');
        return parent::beforeDelete();
    }

}
