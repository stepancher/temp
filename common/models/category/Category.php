<?php

namespace common\models\category;

use common\helpers\ArrayHelper;
use quartz\tools\behaviors\AllToListBehavior;
use quartz\tools\helpers\AllToListHelper;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "category".
 *
 * @property integer $id asdadas
 * @property string $name
 * @property string $is_show
 * @property string $is_default
 * @property string $created_at
 * @property string $updated_at
 */
class Category extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_show', 'is_default'], 'boolean'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('category', 'ID'),
            'name' => Yii::t('category', 'Name'),
            'is_show' => Yii::t('category', 'Is show'),
            'is_default' => Yii::t('category', 'Is default checked'),
            'created_at' => Yii::t('category', 'Created At'),
            'updated_at' => Yii::t('category', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    private static function getShortDataAttributes()
    {
        return [
            'id',
            'name',
            'is_default'
        ];
    }

    /**
     * Данные для отдачи по апи
     * @return array
     */
    public function getShortData()
    {
        return $this->getAttributes(self::getShortDataAttributes());
    }

    /**
     * Список всех элементов модели
     */
    public static function getAllToList()
    {
        return AllToListHelper::getAllToList('id', self::getShortDataAttributes(), 'category_list', 'category', self::className(), 'array');
    }
}
