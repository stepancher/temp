<?php

namespace common\models\points;

use common\models\user\User;
use common\services\user\UserService;
use quartz\tools\validators\MaskValidator;
use Yii;
use yii\base\Model;
use yii\elasticsearch\Exception;

/**
 * This is the model class for index "place".
 *
 * @property string name
 * @property string short_description
 * @property string description
 * @property integer id_user
 * @property integer begin_date
 * @property integer duration
 * @property integer id_city
 * @property string address
 * @property string address_notes
 * @property boolean is_with_child
 * @property boolean is_free
 * @property array tags
 * @property integer code_org
 * @property boolean is_approved
 * @property boolean is_im_org
 * @property boolean is_parser
 * @property array categories
 * @property array location
 * @property integer created_at
 * @property integer updated_at
 * @property string code
 *
 */
class PlaceForm extends Model
{
    public function __construct(Place $place = null, $scenario = 'default')
    {
        $this->setScenario($scenario);
        if (!is_null($place)) {
            $this->setAttributes($place->getAttributes());
            $this->_id = $place->getId();
            if ('backend' === $this->scenario) {
                $this->begin_date = Yii::$app->formatter->asDatetime($place->begin_date, 'Y-m-d HH:MM');
            }
        }
        if ('api' === $this->scenario) {
            $this->id_user = Yii::$app->user->getId();
        }


    }

    public $_id, $duration,
        $name, $short_description, $description, $id_user,
        $begin_date, $id_city, $address,
        $address_notes, $is_with_child, $is_free,
        $tags, $code_org, $is_approved, $is_parser, $categories,
        $views, $created_at, $updated_at, $is_im_org, $location;

    /**
     * Короч если все збс не валидировать можно хапнуть писец каких проблем и пересчетов индексов,
     * что в теории на bigdata может положить всю систему, так что рукожопам в этот метод лазить нехер!
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes', 'code_org',
            ], 'filter', 'filter' => 'trim'],
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes', 'code_org',
            ], 'filter', 'filter' => 'strip_tags'],
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes', 'code_org',
            ], 'string'],
            [['begin_date'], MaskValidator::className(),
                'mask' => 'XXXX-XX-XX XX:XX', 'message' => Yii::t('auth', 'Input format') . ' XXXX-XX-XX XX:XX'
            ],
            [['name', 'short_description', 'description', 'id_user',
                'begin_date', 'id_city', 'address',
                'is_with_child', 'is_free', 'categories',
            ], 'required', 'strict' => true],
            [['code_org'], 'required', 'strict' => true, 'when' => function ($model) {
                return $model->is_im_org != true;
            },],
            [['id_user', 'id_city', 'views', 'duration'
            ], 'number', 'integerOnly' => true],
            [['is_with_child', 'is_free', 'is_approved', 'is_parser', 'is_im_org'
            ], 'boolean'],
            [['categories'], 'each', 'rule' => ['integer'], 'message' => Yii::t('point', 'Category must be array')],
            [['tags'], 'each', 'rule' => ['string']],
            /** todo валидация location*/
            [['location'], 'each', 'rule' => [MaskValidator::className(),
                'mask' => 'XX.XXXXX', 'message' => Yii::t('auth', 'Input format') . ' XX.XXXXX']
            ],
            [
                'code', 'exist',
                'targetClass' => Yii::$app->getModule('user')->model('User', [])->classname(),
                'targetAttribute' => ['code' => 'code'],
                'message' => \Yii::t('auth', 'A user with this code does not exist.')
            ],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => [
            ],
            'backend' => [
                'duration', 'name', 'short_description', 'description', 'id_user',
                'begin_date', 'id_city', 'address',
                'address_notes', 'is_with_child', 'is_free',
                'tags', 'id_org', 'is_approved', 'categories',
                'views', 'location'

            ],
            'api' => [
                'duration', 'name', 'short_description', 'description',
                'begin_date', 'id_city', 'address', 'address_notes',
                'is_with_child', 'is_free', 'tags', 'categories',
                'views', 'is_im_org',
                'code_org',
                'location'
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            (new Community)->attributeLabels(),
            [
                'is_im_org' => Yii::t('point', 'I\'m org'),
            ]
        );
    }

    /**
     * Создание мероприятия
     */
    public function save($place = null)
    {
        try {
            if (is_null($place)) {
                $place = new Place();
            }

            $model->setAttributes($this->getAttributes());
            /** Если установлена галка "я организатор" то пишем орга текущего пользователя */
            if ($this->is_im_org) {
                $model->id_org = Yii::$app->user->getId();
            } else {
                $model->id_org = UserService::getUserIdByCode($this->code_org);
            }

            /**
             * todo : костыль вколоченный во время
             */
            $place->begin_date = $this->begin_date . ':00.000';


            if ($place->save()) {
                return $place->getId();
            } else {
                Yii::error(['Ошибка валидации', $place->errors], 'elasticsearch');
                return false;
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'elasticsearch');
            $this->addError('_id', Yii::t('app', 'oops something went wrong'));
            return false;
        }
    }
}
