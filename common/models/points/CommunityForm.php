<?php

namespace common\models\points;

use common\models\user\User;
use common\services\user\UserService;
use quartz\tools\validators\MaskValidator;
use Yii;
use yii\base\Model;
use yii\elasticsearch\Exception;

/**
 * This is the form class for index "community".
 *
 * @property string name
 * @property string short_description
 * @property string description
 * @property integer id_user
 * @property integer begin_date
 * @property integer duration
 * @property integer id_city
 * @property string address
 * @property string address_notes
 * @property boolean is_with_child
 * @property boolean is_free
 * @property array tags
 * @property integer code_org
 * @property boolean is_approved
 * @property boolean is_im_org
 * @property boolean is_parser
 * @property array categories
 * @property array location
 * @property integer created_at
 * @property integer updated_at
 * @property string code
 *
 */
class CommunityForm extends Model
{
    public function __construct(Community $model = null, $scenario = 'default')
    {
        $this->setScenario($scenario);
        if (!is_null($model)) {
            $this->setAttributes($model->getAttributes());
            $this->_id = $model->getId();
            if ('backend' === $this->scenario) {
                $this->begin_date = Yii::$app->formatter->asDatetime($model->begin_date, 'Y-m-d HH:MM');
            }
        }
        if ('api' === $this->scenario) {
            $this->id_user = Yii::$app->user->getId();
        }


    }

    public $_id, $duration,
        $name, $short_description, $description, $id_user,
        $begin_date, $id_city, $address,
        $address_notes, $is_with_child, $is_free,
        $tags, $code_org, $is_approved, $is_parser, $categories,
        $views, $created_at, $updated_at, $is_im_org, $location;

    /**
     * Короч если все збс не валидировать можно хапнуть писец каких проблем и пересчетов индексов,
     * что в теории на bigdata может положить всю систему, так что рукожопам в этот метод лазить нехер!
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes', 'code_org',
            ], 'filter', 'filter' => 'trim'],
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes', 'code_org',
            ], 'filter', 'filter' => 'strip_tags'],
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes', 'code_org',
            ], 'string'],
            [['begin_date'], MaskValidator::className(),
                'mask' => 'XXXX-XX-XX XX:XX', 'message' => Yii::t('auth', 'Input format') . ' XXXX-XX-XX XX:XX'
            ],
            [['name', 'short_description', 'description', 'id_user',
                'begin_date', 'id_city', 'address',
                'is_with_child', 'is_free', 'categories',
            ], 'required', 'strict' => true],
            [['code_org'], 'required', 'strict' => true, 'when' => function ($model) {
                return $model->is_im_org != true;
            },],
            [['id_user', 'id_city', 'views', 'duration'
            ], 'number', 'integerOnly' => true],
            [['is_with_child', 'is_free', 'is_approved', 'is_parser', 'is_im_org'
            ], 'boolean'],
            [['categories'], 'each', 'rule' => ['integer'], 'message' => Yii::t('point', 'Category must be array')],
            [['tags'], 'each', 'rule' => ['string']],
            /** todo валидация location*/
            [['location'], 'each', 'rule' => [MaskValidator::className(),
                'mask' => 'XX.XXXXX', 'message' => Yii::t('auth', 'Input format') . ' XX.XXXXX']
            ],
            [
                'code', 'exist',
                'targetClass' => Yii::$app->getModule('user')->model('User', [])->classname(),
                'targetAttribute' => ['code' => 'code'],
                'message' => \Yii::t('auth', 'A user with this code does not exist.')
            ],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => [
            ],
            'backend' => [
                'duration', 'name', 'short_description', 'description', 'id_user',
                'begin_date', 'id_city', 'address',
                'address_notes', 'is_with_child', 'is_free',
                'tags', 'id_org', 'is_approved', 'categories',
                'views', 'location'

            ],
            'api' => [
                'duration', 'name', 'short_description', 'description',
                'begin_date', 'id_city', 'address', 'address_notes',
                'is_with_child', 'is_free', 'tags', 'categories',
                'views', 'is_im_org',
                'code_org',
                'location'
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            (new Community)->attributeLabels(),
            [
                'is_im_org' => Yii::t('point', 'I\'m org'),
            ]
        );
    }

    /**
     * Создание сообщества
     */
    public function save($model = null)
    {
        try {
            if (is_null($model)) {
                $model = new Community();
            }

            $model->setAttributes($this->getAttributes());
            /** Если установлена галка "я организатор" то пишем орга текущего пользователя */
            if ($this->is_im_org) {
                $model->id_org = Yii::$app->user->getId();
            } else {
                $model->id_org = UserService::getUserIdByCode($this->code_org);
            }

            /**
             * todo : костыль вколоченный во время
             */
            $model->begin_date = $this->begin_date . ':00.000';


            if ($model->save()) {
                return $model->getId();
            } else {
                Yii::error(['Ошибка валидации', $model->errors], 'elasticsearch');
                return false;
            }
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'elasticsearch');
            $this->addError('_id', Yii::t('app', 'oops something went wrong'));
            return false;
        }
    }
}
