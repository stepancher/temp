<?php

namespace common\models\points;

use common\models\BaseElasticModel;
use common\models\category\Category;
use common\models\user\User;
use quartz\tools\validators\MaskValidator;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the base model class for table "point".
 *
 * @property string name
 * @property string short_description
 * @property string description
 * @property integer id_user
 * @property integer begin_date
 * @property integer duration
 * @property integer id_city
 * @property string address
 * @property string address_notes
 * @property array location
 * @property boolean is_with_child
 * @property boolean is_free
 * @property array tags
 * @property integer id_org
 * @property integer status
 * @property boolean is_approved
 *
 * @property boolean is_parser
 * @property array categories
 * @property integer created_at
 * @property integer updated_at
 *
 */
class BasePoint extends BaseElasticModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_ARCHIVE = 2;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->primaryKey;
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_ARCHIVE => 'Архивный',
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        return isset(self::getStatusLabels()[$this->status]) ? self::getStatusLabels()[$this->status] : '';
    }

    /**
     * @return string
     */
    public static function index()
    {
        return "point";
    }

    /**
     * @return string
     */
    public static function type()
    {
        return "";
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() =>
                [
                    'properties' => [
                        'name' => [
                            'type' => 'string',
                            "fielddata" => [
                                "format" => "disabled"
                            ],
                            "index" => "analyzed",
                            "omit_norms" => true,
                            "fields" => [
                                "raw" => [
                                    "ignore_above" => 4096,
                                    "index" => "not_analyzed",
                                    "type" => "string"
                                ]
                            ],
                        ],
                        'short_description' => [
                            'type' => 'string',
                            "fielddata" => [
                                "format" => "disabled"
                            ],
                            "index" => "analyzed",
                            "omit_norms" => true,
                            "fields" => [
                                "raw" => [
                                    "ignore_above" => 4096,
                                    "index" => "not_analyzed",
                                    "type" => "string"
                                ]
                            ],
                        ],
                        'description' => [
                            "type" => "string",
                            "fielddata" => [
                                "format" => "disabled"
                            ],
                            "index" => "analyzed",
                            "omit_norms" => true,
                            "fields" => [
                                "raw" => [
                                    "ignore_above" => 4096,
                                    "index" => "not_analyzed",
                                    "type" => "string"
                                ]
                            ],
                        ],
                        'id_user' => ['type' => 'long'],
                        'begin_date' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss.SSS'
                        ],
                        'duration' => ['type' => 'long'],
                        'id_city' => ['type' => 'long'],
                        'address' => [
                            'type' => 'string',
                            "fielddata" => [
                                "format" => "disabled"
                            ],
                            "index" => "analyzed",
                            "omit_norms" => true,
                            "fields" => [
                                "raw" => [
                                    "ignore_above" => 4096,
                                    "index" => "not_analyzed",
                                    "type" => "string"
                                ]
                            ],],
                        'address_notes' => [
                            'type' => 'string',
                            "fielddata" => [
                                "format" => "disabled"
                            ],
                            "index" => "analyzed",
                            "omit_norms" => true,
                            "fields" => [
                                "raw" => [
                                    "ignore_above" => 4096,
                                    "index" => "not_analyzed",
                                    "type" => "string"
                                ]
                            ],],
                        'location' => ['type' => 'geo_point', 'lat_lon' => true],
                        'is_with_child' => ['type' => 'boolean'],
                        'is_free' => ['type' => 'boolean'],
                        'tags' => ['type' => 'string'],
                        'id_org' => ['type' => 'long'],
                        'is_approved' => ['type' => 'boolean'],
                        'is_parser' => ['type' => 'boolean'],
                        'categories' => ['type' => 'long'],
                        'views' => ['type' => 'long'],
                        'created_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss.SSS'
                        ],
                        'updated_at' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss.SSS'
                        ],
                        'status' => ['type' => 'long'],
                    ]
                ],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            'duration', 'status',
            'name', 'short_description', 'description', 'id_user',
            'begin_date', 'id_city', 'address',
            'address_notes', 'location', 'is_with_child', 'is_free',
            'tags', 'id_org', 'is_approved', 'is_parser', 'categories',
            'views', 'created_at', 'updated_at'
        ];
    }


    /**
     * Короч если все збс не валидировать можно хапнуть писец каких проблем и пересчетов индексов,
     * что в теории на bigdata может положить всю систему, так что рукожопам в этот метод лазить нехер!
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes',
            ], 'filter', 'filter' => 'trim'],
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes',
            ], 'filter', 'filter' => 'strip_tags'],
            [[
                'name', 'short_description', 'description', 'address',
                'address_notes',
            ], 'string'],
            [['begin_date'], MaskValidator::className(),
                'mask' => 'XXXX-XX-XX XX:XX:XX.XXX', 'message' => Yii::t('auth', 'Input format') . ' XXXX-XX-XX XX:XX:XX.XXX'
            ],
            [['name', 'short_description', 'description', 'id_user',
                'begin_date', 'id_city', 'address', 'location',
                'is_with_child', 'is_free', 'id_org', 'categories',
            ], 'required'],
            [['id_user', 'id_city', 'id_org', 'views', 'duration', 'status'
            ], 'number', 'integerOnly' => true],
            [['is_with_child', 'is_free', 'is_approved', 'is_parser'
            ], 'boolean'],
            [['categories'], 'each', 'rule' => ['integer']],
            [['tags'], 'each', 'rule' => ['string']],
            [['location'], 'each', 'rule' => [ MaskValidator::className(),
                'mask' => 'XX.XXXXX', 'message' => Yii::t('auth', 'Input format') . ' XX.XXXXX']
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => date('Y-m-d H:i:s.Z')
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('point', 'Name'),
            'short_description' => Yii::t('point', 'Short Description'),
            'description' => Yii::t('point', 'Description'),
            'id_user' => Yii::t('point', 'User'),
            'begin_date' => Yii::t('point', 'Date begin'),
            'id_city' => Yii::t('point', 'City'),
            'address' => Yii::t('point', 'Adress'),
            'address_notes' => Yii::t('point', 'Adress Notes'),
            'location' => Yii::t('point', 'Location'),
            'is_with_child' => Yii::t('point', 'Is with child'),
            'is_free' => Yii::t('point', 'Is free'),
            'tags' => Yii::t('point', 'Tags'),
            'id_org' => Yii::t('point', 'Organizer'),
            'is_approved' => Yii::t('point', 'Is approved'),
            'is_parser' => Yii::t('point', 'Is parser'),
            'categories' => Yii::t('point', 'Categories'),
            'views' => Yii::t('point', 'Views'),
            'duration' => Yii::t('point', 'Duration'),
            'status' => Yii::t('point', 'Status'),
            'created_at' => Yii::t('point', 'Created At'),
            'updated_at' => Yii::t('point', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getCategoriesModels()
    {
        return $this->hasMany(Category::className(), ['id' => 'categories']);
    }

    /**
     * @return string
     */
    public function getCategoriesNamesString()
    {
        return implode(', ', $this->getCategoriesModels()->select('name')->column());
    }

    /**
     * @return string
     */
    public function getTagsString()
    {
        return !empty($this->tags) ? implode(', ', $this->tags) : '';
    }

    /**
     * Проверка, может ли пользоатель обнолять мероприятие
     * @return bool
     */
    public function checkCanUpdate()
    {
        return $this->id_user == Yii::$app->user->getId();
    }

    /**
     * Поля которые будут отданы по api
     * @return array
     */
    private static function getShortDataAttributes()
    {
        return [
            'duration',
            'name', 'short_description', 'description', 'id_user',
            'begin_date', 'id_city', 'address',
            'address_notes', 'location', 'is_with_child', 'is_free',
            'tags', 'id_org', 'categories',
            'views'
        ];
    }

    /**
     * Данные о точке, которые мы предоставляем всем пользователям
     */
    public function getShortData()
    {
        $shortData = $this->getAttributes(self::getShortDataAttributes());
//        $shortData['avatar'] = $this->getProfileAvatar('380x255');
        return $shortData;
    }
}
