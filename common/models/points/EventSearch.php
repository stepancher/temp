<?php

namespace common\models\points;

use common\exceptions\ApiException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\BadRequestHttpException;

/**
 * EventSearch represents the model behind the search form about `common\models\points\Event`.
 */
class EventSearch extends Event
{
    public $top_left, $bottom_right;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'name', 'short_description', 'description', 'id_user', 'begin_date',
                'id_city', 'address', 'address_notes', 'location', 'is_with_child', 'is_free', 'tags',
                'id_org', 'is_approved', 'is_parser', 'categories', 'views', 'created_at', 'updated_at',
                'status', 'top_left', 'bottom_right'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => [
                'name', 'short_description', 'description', 'id_user', 'begin_date', 'duration',
                'id_city', 'address', 'address_notes', 'location', 'is_with_child', 'is_free', 'tags',
                'id_org', 'is_approved', 'is_parser', 'categories', 'views', 'created_at', 'updated_at',
                'status'
            ],
            'front' => [
                'name', 'short_description', 'description', 'id_user',
                'id_city', 'address', 'address_notes', 'is_with_child', 'is_free', 'tags',
                'id_org', 'is_approved', 'is_parser', 'categories', 'status', 'top_left', 'bottom_right'
            ]
        ];
    }

    public function attributes()
    {
        $attributes = parent::attributes();
        $attributes[] = 'top_left';
        $attributes[] = 'bottom_right';
        return $attributes;
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return BaseDataProvider
     */
    public function search($params)
    {

        $this->load($params);

        if (!$this->validate()) {
            throw new BadRequestHttpException('Ошибка при валидации поиска', ApiException::API_VALIDATION_ERROR);
        }
//        $query = new Query();
//        $query->source('*');
//        $query->from(Event::index(), Event::type());

        $query = Event::find();

        $query
            ->andFilterWhere(['id_user' => $this->id_user])
            ->andFilterWhere(['id_city' => $this->id_city])
            ->andFilterWhere(['id_org' => $this->id_org])
            ->andFilterWhere(['duration' => $this->duration])
            ->andFilterWhere(['is_free' => $this->is_free])
            ->andFilterWhere(['is_approved' => $this->is_approved])
            ->andFilterWhere(['is_parser' => $this->is_parser])
            ->andFilterWhere(['is_with_child' => $this->is_with_child])
            ->andFilterWhere(['in', 'categories', $this->categories])

//            ->andFilterWhere(['name' => $this->name])
//            ->andFilterWhere(['like', 'short_description', $this->short_description])
//            ->andFilterWhere(['like', 'description', $this->description])
//            ->andFilterWhere(['like', 'address', $this->address])
//            ->andFilterWhere(['like', 'address_notes', $this->address_notes])

//            ->andFilterWhere(['like', 'location', $this->location])

//            ->andFilterWhere(['like', 'views', $this->views])

//            ->andFilterWhere(['like', 'begin_date', $this->begin_date])
//            ->andFilterWhere(['like', 'created_at', $this->created_at])
//            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
        ;


//        var_dump($query->all()[0]);
//        die();


//        $query->orderBy = ['id' => ['order' => 'desc']];


//        $query->addAggregation("status", "terms", array("field" => "status", "order" => ['_term' => "asc"]));
//        $query->addAggregation("author_name", "terms", array("field" => "author_name", 'size' => 25));


//        $filters = [];

//nested filters
//        $filter = [];
//        $filter['nested']['path'] = 'suppliers';
//        $filter['nested']['query']['bool']['must'][]['match']['suppliers.name'] = 'XYZ';


        $filter = $query->filter;

        if (!empty($this->name)) {
            $filter['bool']['must'][]['match']['name'] = $this->name;
        }
        if (!empty($this->description)) {
            $filter['bool']['must'][]['match']['description'] = $this->description;
        }
        if (!empty($this->short_description)) {
            $filter['bool']['must'][]['match']['short_description'] = $this->short_description;
        }
        if (!empty($this->address)) {
            $filter['bool']['must'][]['match']['address'] = $this->address;
        }
        if (!empty($this->address_notes)) {
            $filter['bool']['must'][]['match']['address_notes'] = $this->address_notes;
        }
        if(!empty($this->tags)){
            $filter['term']['tags'] = urlencode('Детское');//$this->tags;
        }

        $query->filter($filter);

        /*** Фильтрация по строкам */
//        $filter['more_like_this']= [
//            'fields' => ['name'],
//            "like" => "%Point%",
//            "min_term_freq" => 1,
//            "max_query_terms" => 12
//        ];

//        if ($queryElastic) {
//            $query->query($queryElastic);
//        }
//
//        $query->orderBy = ['name' => ['order' => 'asc']];
        $page = 1;
        $limit = 10;
        try {
//            $pages = new Pagination(['totalCount' => $query->count(), "defaultPageSize" => $limit]);
            $offset = ($page - 1) * $limit;
            $query->offset($offset)->limit($limit);

            $rows = $query->all();
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'elasticsearch');
            throw new ApiException(Yii::t('app', 'oops something went wrong'), 500);
        }
//        $aggregations = $rows['aggregations'];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $rows,
            'pagination' => [
                'pageSize' => $limit,
            ],
        ]);

        return $dataProvider;
    }
}
