<?php

namespace common\models\points;

use Yii;


class Place extends BasePoint
{
    /**
     * @return string
     */
    public static function type()
    {
        return "place";
    }
}
