<?php

namespace common\models\points;

use Yii;


class Community extends BasePoint
{
    /**
     * @return string
     */
    public static function type()
    {
        return "community";
    }
}
