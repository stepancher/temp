<?php

namespace common\models\points;

class Event extends BasePoint
{
    /**
     * @return string
     */
    public static function type()
    {
        return "event";
    }
}
