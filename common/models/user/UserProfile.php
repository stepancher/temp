<?php
namespace common\models\user;

use Yii;

/**
 * UserProfile model
 *
 *
 * @property string $site
 * @property string $about_me
 * @property string $is_allergy
 * @property string $is_disability
 */
class UserProfile extends \quartz\user\models\UserProfile
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'middlename', 'lastname', 'notes', 'skype', 'date_birthday', 'phone_number'], 'filter', 'filter' => 'trim'],
            [['firstname', 'middlename', 'lastname', 'notes', 'skype', 'date_birthday', 'phone_number'], 'filter', 'filter' => 'strip_tags'],

            [['id_user', 'is_allergy', 'is_disability'], 'integer'],
            [['date_birthday'], 'safe'],
            [['notes', 'phone_number', 'site', 'about_me'], 'string'],
            [['sex'], 'number', 'max' => 1, 'min' => 0, 'integerOnly' => true],
            [['notes', 'skype', 'site', 'about_me'], 'string', 'max' => 255, 'tooLong' => \Yii::t('auth', 'maximum character', ['n' => 255])],
            ['avatar', 'filter', 'filter' => function($str){return preg_match('/^[0-9a-f]+\.(png|jpg|jpeg|svg|gif)$/',$str) ?  $str : null;}],
        ];
    }

    /**
     * Поля которые будут отданы по api
     * @return array
     */
    private static function getShortDataAttributes()
    {
        return [
            'firstname',
            'middlename',
            'lastname',
            'date_birthday',
            'site',
            'about_me',
            'is_allergy',
            'is_disability',
            'sex',
        ];
    }

    /**
     * Данные о пользователе, которые мы предоставляем всем пользователям
     */
    public function getShortData()
    {
        $shortData = $this->getAttributes(self::getShortDataAttributes());
        $shortData['avatar'] = $this->getProfileAvatar('380x255');
        return $shortData;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('auth', 'Id User'),
            'date_birthday' => Yii::t('auth', 'Date Birthday'),
            'sex' => Yii::t('auth', 'Sex'),
            'avatar' => Yii::t('auth', 'Avatar'),
            'skype' => Yii::t('auth', 'Skype'),
            'notes' => Yii::t('auth', 'Notes'),
            'phone_number' => Yii::t('auth', 'Phone number'),
        ];
    }

    /**
     * Возвращает аватар пользователя
     * @return string
     */
    public function getProfileAvatar($size=null)
    {
        if ($this->avatar != '') {
            if($image = $this->getUri($size, true, true)){
                return $image;
            }
        }
        if ($this->sex == User::SEX_MALE) {
            return '/upload/default/no-avatar-male.gif';
        }
        return '/upload/default/no-avatar-female.gif';
    }

}
