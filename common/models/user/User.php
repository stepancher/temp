<?php
namespace common\models\user;

use common\behaviors\ReferalBehavior;
use common\widgets\SettingsPrint;
use Yii;
use yii\data\BaseDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

/**
 * User model
 * @property integer $code
 * @property integer $ref
 * @property integer $id_language
 * @property integer $id_city
 */
class User extends \quartz\user\models\User
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = [
            'referal' => [
                'class' => ReferalBehavior::className(),
                'solt' => 'tetrusbest',
            ],
        ];
        return array_merge($behaviors, parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'filter', 'filter' => 'trim'],
            [['id_city', 'id_language'], 'integer'],
            [['username', 'email'], 'filter', 'filter' => 'strip_tags'],
            ['status', 'default', 'value' => self::STATUS_NOACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_ACTIVE, self::STATUS_NOACTIVE, self::STATUS_BANNED]],
        ];
    }

    /**
     * Поля которые будут отданы по api
     * @return array
     */
    private static function getShortDataAttributes()
    {
        return [
            'code',
            'email',
            'username',
            'id_city',
            'id_language',
        ];
    }

    public function getShortData()
    {
        return $this->getAttributes(self::getShortDataAttributes());
    }

    /**
     * Возвращает модель по серийному номеру
     * @param $code
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function findOneByCode($code)
    {
        return self::find()->where(['code' => $code])->one(); //todo: сейчас по id будет по некому номеру вклада
    }


    public function attributeLabels()
    {
        $result = [
            'code' => \Yii::t('auth', 'Code'),
            'id_referer' => \Yii::t('auth', 'Referer'),
        ];
        return array_merge(parent::attributeLabels(), $result);
    }

    /**
     * Настройки оповещения пользователя
     * @return ActiveQuery
     */
    public function getNotification()
    {
        return $this->hasOne(UserNotificationElastic::className(), ['id_user' => 'id']);
    }

    /**
     * Категории массивом
     * @return string
     */
    public function getFavoriteCategories()
    {
        return $this->notification ? $this->notification->categories : [];
    }
}
