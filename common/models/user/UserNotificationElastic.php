<?php

namespace common\models\user;

use common\models\BaseElasticModel;
use common\models\category\Category;
use quartz\tools\validators\MaskValidator;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for index "user-notify".
 *
 * @property string id_user
 * @property string level
 * @property string whatsapp_number
 * @property string last_notify
 * @property array categories
 *
 */
class UserNotificationElastic extends BaseElasticModel
{

    public function getId()
    {
        return $this->primaryKey;
    }

    public static function index()
    {
        return "user-notify";
    }

    public static function type()
    {
        return "user-notify";
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() =>
                [
                    'properties' => [
                        'id_user' => ['type' => 'long'],
                        'whatsapp_number' => [
                            'type' => 'string',
                            "fielddata" => [
                                "format" => "disabled"
                            ],
                            "index" => "analyzed",
                            "omit_norms" => true,
                            "fields" => [
                                "raw" => [
                                    "ignore_above" => 4096,
                                    "index" => "not_analyzed",
                                    "type" => "string"
                                ]
                            ],
                        ],
                        'level' => ['type' => 'long'],
                        'categories' => ['type' => 'long'],
                        'last_notify' => [
                            'type' => 'date',
                            'format' => 'yyyy-MM-dd HH:mm:ss.SSS'
                        ],
                    ],
                ],
        ];
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            'id_user', 'categories', 'level', 'last_notify', 'whatsapp_number'
        ];
    }


    /**
     * Короч если все збс не валидировать можно хапнуть писец каких проблем и пересчетов индексов,
     * что в теории на bigdata может положить всю систему, так что рукожопам в этот метод лазить нехер!
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['whatsapp_number'], MaskValidator::className(),
                'mask' => '+X-XXX-XXX-XXXX', 'message' => Yii::t('auth', 'Input format') . '+X-XXX-XXX-XXXX'
            ],
            [['last_notify'], MaskValidator::className(),
                'mask' => 'XXXX-XX-XX XX:XX:XX.XXX', 'message' => Yii::t('auth', 'Input format') . ' XXXX-XX-XX XX:XX:XX.XXX'
            ],
            [['id_user', 'categories', 'level'], 'required'],
            [['id_user', 'level'], 'number', 'integerOnly' => true],
            [['categories'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'whatsapp_number' => Yii::t('auth', 'Whatsapp number'),
            'id_user' => Yii::t('auth', 'User'),
            'categories' => Yii::t('auth', 'Categories'),
            'level' => Yii::t('auth', 'Level notify'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getCategoriesModels()
    {
        return $this->hasMany(Category::className(), ['id' => 'categories']);
    }

    /**
     * @return string
     */
    public function getCategoriesNamesString()
    {
        return implode(', ', $this->getCategoriesModels()->select('name')->column());
    }
}
