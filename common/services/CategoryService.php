<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 14:18
 */

namespace common\services;

use common\models\category\Category;
use common\models\user\User;
use Yii;

/**
 * Сервис для работы с настройками
 * Class CategoryService
 * @package common\services
 */
class CategoryService
{
    /**
     * Значение одной конкретной категории сайта
     * @param null $id
     * @return null|string
     */
    public static function getCategoryData($id = null)
    {
        /** @var Category $category */
        $category = Category::findOne($id);
        return is_object($category) ? $category->getShortData() : null;
    }

    /**
     * Все настройки сайта кучкой
     * @return array
     */
    public static function getAllToList()
    {
        return Category::getAllToList();
    }

    /**
     * Предпочитаемые категории пользователя
     * @param User $user
     * @return mixed
     */
    public static function getCategoryByUser(User $user)
    {
        return $user->getFavoriteCategories();
    }



}
