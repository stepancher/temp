<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 14:18
 */

namespace common\services;

use common\exceptions\ApiException;
use quartz\imperavi\helpers\FileHelper;
use yii\base\DynamicModel;
use yii\web\UploadedFile;

/**
 * Сервис для работы с картинками
 * Class UploadService
 * @package common\services
 */
class UploadService
{
    /**
     * Все настройки сайта
     * @return array
     */
    public static function saveImage()
    {
        $path = FileHelper::normalizePath(\Yii::getAlias(\Yii::$app->params['uploadTempDir'])) . DIRECTORY_SEPARATOR;
        if (!FileHelper::createDirectory($path)) {
            throw new ApiException(ApiException::API_GENERAL_ERROR, \Yii::t('app', 'ERROR_CAN_NOT_UPLOAD_FILE'));
        }

        $file = UploadedFile::getInstanceByName('file');
        $model = new DynamicModel(compact('file'));
        $model->addRule('file', 'image', ['extensions' => 'png, jpeg, jpg, gif', 'checkExtensionByMimeType' => false])->validate();
        if ($model->hasErrors()) {
            throw new ApiException(ApiException::API_GENERAL_ERROR, $model->getFirstError('file'));
        } else {
            if (null !== $model->file) {
                if ($model->file->extension) {
                    $model->file->name = uniqid() . '.' . $model->file->extension;
                }
                if ($model->file->saveAs($path . $model->file->name)) {
                    return [
                        'status' => 'OK',
                        'message' => \Yii::t('app', 'The image is saved successfully'),
                        'name' => $model->file->name
                    ];
                } else {
                    throw new ApiException(ApiException::API_GENERAL_ERROR, \Yii::t('app', 'ERROR_CAN_NOT_UPLOAD_FILE'));
                }
            } else {
                throw new ApiException(ApiException::API_GENERAL_ERROR, \Yii::t('app', 'ERROR_CAN_NOT_UPLOAD_FILE'));
            }
        }
    }
}
