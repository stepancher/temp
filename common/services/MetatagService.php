<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 14:18
 */

namespace common\services;

use common\exceptions\ApiException;
use quartz\metatag\models\MetaTag;
use quartz\settingsModule\models\Settings;
use Yii;
use yii\caching\TagDependency;

/**
 * Сервис для работы с метатегами
 * Class MetatagService
 * @package common\services
 */
class MetatagService
{
    /**
     * Возвращает метатеги по url
     * @param null $code
     * @return null|string
     */
    public static function getMetaTagByUrl($url = null)
    {
        $fields = [
            'title',
            'keywords',
            'description',
            'others',
            'url'
        ];
        $metaTag = Yii::$app->db->cache(function ($db) use ($url, $fields) {
            return MetaTag::find()->select($fields)->where(['url' => $url])->asArray()->one();
        }, Yii::$app->getModule('metatag')->timeCache, new TagDependency(['tags' => 'metatag']));

        if(null === $metaTag){
            throw new ApiException(ApiException::API_ERROR_NO_CONTENT);
        }

        return ['metatag' => $metaTag];
    }
}
