<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.04.16
 * Time: 14:18
 */

namespace common\services\auth;

use api\models\user\LoginForm;
use api\models\user\SignupForm;
use common\exceptions\ApiException;
use common\services\CategoryService;
use Yii;
use yii\web\Request;

/**
 * Сервис для работы к аутентификацией пользователя
 * Class AuthService
 * @package common\services\auth
 */
class AuthService
{
    /**
     * Аутентификация пользователя по логину и паролю
     * так же проверяет его наличие у нас в системе.
     * @param Request $request Данные сто пришли в Post запросе
     * @return array Массив аутентификационных данных пользователя
     */
    public function signin(Request $request, \yii\web\User $user)
    {
        //todo если уже зарегистрирован?
        if ($user->isGuest) {
            $loginForm = new LoginForm();
            $loginForm->load($request->post(), '');
            if ($loginForm->validate()) { //загружаем данные пост, и пробуем залогиниться
                $user->login($loginForm->getUser(), Yii::$app->getModule('user')->autoLoginDuration);
            } else {
                throw new ApiException(ApiException::API_VALIDATION_ERROR, null, $loginForm->errors);
            }
        }
        return [
            'user' => $user->identity->getShortData(),
            'favoriteCategories' => CategoryService::getCategoryByUser($user->identity)
        ];
    }

    /**
     * Регистрация пользователя
     * так же проверяет его наличие у нас в системе.
     * @param Request $request Данные что пришли в Post запросе
     * @return array Массив данных пользователя
     */
    public function signup(Request $request, \yii\web\User $user)
    {
        if (Yii::$app->user->isGuest) {
            //todo слишком частая регистрация checkFrequentSignup
            $signupForm = new SignupForm();
            if ($signupForm->load($request->post(), '')) {
                if ($signupForm->validate()) { //загружаем данные пост, и пробуем залогиниться
                    if ($userModel = $signupForm->signup()) {
                        $user->login($userModel, Yii::$app->getModule('user')->autoLoginDuration);
                        return [
                            'user' => $user->identity->getShortData(),
                            'favoriteCategories' => CategoryService::getCategoryByUser($user->identity)
                        ];
                    } else {
                        throw new ApiException(ApiException::API_GENERAL_ERROR);
                    }
                } else {
                    throw new ApiException(ApiException::API_VALIDATION_ERROR, null, $signupForm->errors);
                }
            }
            throw new ApiException(ApiException::API_GENERAL_ERROR, Yii::t('app', 'Missing post data'));
        } else {
            throw new ApiException(ApiException::API_GENERAL_ERROR, Yii::t('auth', 'You already signin'));
        }
    }


    /**
     * Проверка на слишком частую регистрацию
     * @return bool|string
     */
    public static function checkFrequentSignup()
    {
        $key = 'user_last_signup_' . \Yii::$app->request->getUserIP();
        if (\Yii::$app->cache->exists($key) and \Yii::$app->cache->get($key) + 60 * 2 > time()) {
            throw new ApiException(ApiException::API_GENERAL_ERROR, Yii::t('auth', 'you can\'t signup more than 1 time in 2 minutes'));

        } else {
            \Yii::$app->cache->delete($key); //удаляем из мемкеша
            return false;
        }
    }

    /**
     * Разлогинить пользователя
     * @return array
     */
    public function logout(\yii\web\User $user)
    {
        if ($user->logout()) {
            return [
                'status' => 'OK',
                'message' => Yii::t('app', 'You have successfully logged out')
            ];
        } else {
            throw new ApiException();
        }
    }
}
