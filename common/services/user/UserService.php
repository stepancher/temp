<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.04.16
 * Time: 14:18
 */

namespace common\services\user;

use api\models\user\LoginForm;
use api\models\user\UserForm;
use api\models\user\UserNotifyForm;
use common\exceptions\ApiException;
use quartz\user\models\User;
use Yii;
use yii\web\Request;

/**
 * Сервис для работы к аутентификацией пользователя
 * Class UserService
 * @package common\services\user
 */
class UserService
{
    /**
     * Редактирование профиля пользователя
     * @param Request $request Данные что пришли запросе
     * @param \yii\web\User $user Данные что пришли запросе
     * @return string|ApiException сообщение об успехе или Exception
     */
    public function updatePersonal(Request $request, \yii\web\User $user)
    {
        if (!$user->isGuest) {
            $userForm = new UserForm();
            $userForm->id = $user->getId();
            /** Заполняем старые данные */
            $userForm->setAttributes($user->identity->getAttributes(['id_city', 'id_language', 'code'])) ;
            $userForm->setAttributes($user->identity->profile->getAttributes([
                'sex', 'skype', 'site', 'about_me',
                'firstname', 'middlename', 'lastname',
                'is_allergy', 'is_disability', 'date_birthday',
                'avatar'
            ])) ;
            /** Лоадим новые */
            $userForm->load($request->post(), '');
            if ($userForm->validate()) { //загружаем данные пост, и пробуем залогиниться
                if ($userForm->update()) {
                    return [
                        'status' => 'OK',
                        'message' => Yii::t('auth', 'User data saved successfully')
                    ];
                } else {
                    throw new ApiException();
                }
            } else {
                throw new ApiException(ApiException::API_VALIDATION_ERROR, null, $userForm->errors);
        }
        }
        throw new ApiException(ApiException::API_AUTHORIZATION_ERROR);
    }

    /**
     * Редактирование уведомлений пользователя
     * @param Request $request Данные что пришли запросе
     * @param \yii\web\User $user Данные что пришли запросе
     * @return string|ApiException сообщение об успехе или Exception
     */
    public function updateNotify(Request $request, \yii\web\User $user)
    {
        if (!$user->isGuest) {
            $userNotifyForm = new UserNotifyForm();
            $userNotifyForm->id_user = $user->getId();
            $userNotifyForm->load($request->post(), '');
            if ($userNotifyForm->validate()) { //загружаем данные пост, и пробуем залогиниться
                if ($userNotifyForm->save()) {
                    return [
                        'status' => 'OK',
                        'message' => Yii::t('auth', 'User data saved successfully')
                    ];
                } else {
                    throw new ApiException();
                }
            } else {
                throw new ApiException(ApiException::API_VALIDATION_ERROR, null, $userNotifyForm->errors);
            }
        }
        throw new ApiException(ApiException::API_AUTHORIZATION_ERROR);
    }

    /**
     * Основная информация о пользователе
     * @param \common\models\user\User $user
     * @return array
     */
    public static function getShortData(\common\models\user\User $user)
    {
        return $user->getShortData();
    }

    public static function getUserIdByCode($code)
    {
        if($id_user = \common\models\user\User::find()->select('id')->where(['code' => $code])->scalar()){
            return $id_user;
        } else {
            throw new ApiException(400, Yii::t('auth', 'Can\'t find user by code'));
        }
    }

}
