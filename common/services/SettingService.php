<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 14:18
 */

namespace common\services;

use quartz\settingsModule\models\Settings;
use quartz\tools\helpers\AllToListHelper;
use Yii;

/**
 * Сервис для работы с настройками
 * Class SettingService
 * @package common\services
 */
class SettingService
{
    /**
     * Значение одной конкретной настройки сайта
     * @post $code string Код запрашиваемой настройки
     * @return null|string
     */
    public static function getSetting($code = null)
    {
        return [
            'setting' => [
                'value' => Settings::getValueByCode($code),
                'code' => $code
            ]
        ];
    }

    /**
     * Все настройки сайта
     * @return array
     */
    public static function getAllSettings()
    {
        return ['settings' => Settings::getAllToList([], true)];
    }
}
