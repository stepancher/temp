<?php

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\Controller;

use common\models\User;

/**
 * Class ReferalBehavior
 * @package common\behaviors
 */
class ReferalBehavior extends Behavior
{
    /**
     * Соль, чтоб не могли вычислить id пользователя методом перебора
     * @var string
     */
    public $solt = 'soltsoltsolt';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'setRefCode',
        ];
    }


    /**
     * Генерирует реферальный код
     * @return string
     */
    public function generateRefCode($id)
    {
        return crc32($this->solt.$id);
    }

    /**
     * Устанавливает реф код
     * @return string
     */
    public function setRefCode()
    {
        $this->owner->code  = $this->generateRefCode($this->owner->id);
        $this->owner->save(false,['code']);
    }

    /**
     * простая Реферальная ссылка
     * @return string
     */
    public function getReferalLink()
    {
        return Yii::$app->urlManager->hostInfo.'/ref/'.$this->owner->code;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferer()
    {
        return $this->hasOne($this->owner->className(), ['id' => 'id_referer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferals()
    {
        return $this->hasMany($this->owner->className(), ['id_referer'=>'id']);
    }
}