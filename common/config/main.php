<?php


return [
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['log'],
    'components' => [
        'db' => require(__DIR__ . '/db.php'),
        'mailer' => require(__DIR__ . '/mail.php'),
        'i18n' => require(__DIR__ . '/i18n.php'),
        'cache' => require(__DIR__ . '/cache.php'),
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\user\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['auth'],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['Guest'],
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.Y',
            'timeFormat' => 'hh:mm:ss',
            'datetimeFormat' => 'd.m.Y H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'nullDisplay' => true
		],
        'session' => [
            'class' => 'yii\web\CacheSession',
            'cache' => 'cache',
        ],
	],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'mailnotify' => [
            'class' => 'vendor\quartz\mailnotify\mailnotify',
            'externalMessageSource' => true
        ],
        'metatag' => [
            'class' => 'quartz\metatag\MetaTag',
            'useCache' => true,
            'timeCache' => 3600,
            'externalMessageSource' => true,
            'excludeUrls' => ['/\/api/i', '/\/admin/i'], // Url'ы для которых не нужно сохранять мета-теги, $pattern - регулярное выражение
        ],
        'blocks' => [
            'class' => 'quartz\blocks\Blocks',
            'useCache' => true,
            'timeCache' => 3600,
            'modelClasses' => [
                'Blocks' => 'backend\models\Blocks'
            ],
            'modelViews' => [
                'update' => '@backend/views/blocks/update'
            ],
            'externalMessageSource' => true
        ],
        'forms' => [
            'class' => 'quartz\forms\Forms',
            'externalMessageSource' => true
        ],
        'pages' => [
            'class' => 'quartz\pages\Pages',
            'useCache' => true,
            'timeCache' => 3600,
            'modelClasses' => [
                'Pages' => 'backend\models\Pages'
            ],
            'modelViews' => [
                'page_edit' => '@backend/views/pages/page_edit'
            ],
            'externalMessageSource' => true
        ],
        'menu' => [
            'class' => 'quartz\menu\Menu',
            'useCache' => true,
            'timeCache' => 3600,
            'externalMessageSource' => true
        ],
        'user' => [
            'class' => 'quartz\user\User',
            'externalMessageSource' => true,
            'autoLoginDuration' => 60*60*24*30,
            'modelClasses' => [
                'User' => 'common\models\user\User',
                'UserProfile' => 'common\models\user\UserProfile'
            ],
            'reCapchaSecret' => '6LcNFQkUAAAAAF72pzuKHt2zHw-ILrrW8PgiDI5m',
            'reCapchaSiteKey' => '6LcNFQkUAAAAAOTv4AWEkD05OckgL95Jw123r0nR'
        ],
        'settings' => [
            'class' => 'quartz\settingsModule\Settings',
            'externalMessageSource' => true,
        ],
        'localization' => [
            'class' => 'quartz\localization\Localization',
            'useCache' => true,
            'timeCache' => 3600,
            'externalMessageSource' => true,
        ],
        'mq' => require(__DIR__ . '/mq.php'),
    ],
];
