<?php

return [

    'translations' => [
        '*' => [
            'class' => 'quartz\localization\i18n\DbMessageSource',
            'db' => 'db',
            'forceTranslation' => true,
            'sourceMessageTable' => 'source_message',
            'messageTable' => 'message',
            'cachingDuration' => 24*60*60, // в секундах
            'enableCaching' => true,
        ],
    ],
];
