<?php
return [
    'adminEmail' => 'admin@terus.club',
    'supportEmail' => 'support@terus.club',
    'user.passwordResetTokenExpire' => 3600*24,

    'languages'  =>  [
        'ru' => 'Русский',
        'en' => 'English',
    ],
    'defaultLanguage' => 'ru',

    'stompUser' => 'admin',
    'stompPassword' => 'password',
    'stompHost' => 'localhost',
    'stompPort' => 61613,
    'stompDestinations' => require(__DIR__ . '/destinationsForActiveMQ.php'),

    'apiStatusOk' => 'OK',
    'apiStatusErr'=> 'ERR',

    'uploadTempDir' => '@root/upload/temp',
];
