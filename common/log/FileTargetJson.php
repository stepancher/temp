<?php
namespace common\log;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\log\FileTarget;
use yii\log\Logger;
use yii\web\JsonResponseFormatter;

/**
 * FileTarget records log messages in a file json format.
 *
 * @author Stepan Cherepanov <qiang.xue@gmail.com>
 */
class FileTargetJson extends FileTarget
{

    /**
     * Форматирует лог в json формат переж записью в файл
     * @param array $message the log message to be formatted.
     * The message structure follows that in [[Logger::messages]].
     * @return string the formatted message
     */

    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;
        $level = Logger::getLevelName($level);
        if ($text instanceof \Throwable || $text instanceof \Exception) {
            $text = (string) $text;
        }

        /**
         * Никаких трейсов в json!
        $traces = [];
        if (isset($message[4])) {
            foreach ($message[4] as $trace) {
                $traces[] = "in {$trace['file']}:{$trace['line']}";
            }
        }
         */

        return Json::encode([
            'timestamp' => date('Y-m-d H:i:s', $timestamp),
            'level' => $level,
            'category' => $category,
            'text' => $text,
        ]);
    }

}
