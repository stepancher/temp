<?php
/**
 * Created by PhpStorm.
 * User: kaiser
 * Date: 14.01.16
 * Time: 10:04
 */

namespace common\exceptions;


use yii\rest\ViewAction;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Это исключение было запилено чтобы негативные апи тесты успешно проходили, так как в модуле Yii2 для
 * кодесепшена через ЭррорХэндлер пропускаются только исключения типа HttpException и его наследники, а остальные
 * просто валят выполнение тестов
 */
class ApiException extends HttpException implements CustomExceptionInterface
{
    /** Константа для логов связанных с HumanFriendlyException*/
    const LOG_CATEGORY = 'common\services\exceptions\HumanFriendlyException';

    /** Стандартное сообщение об ошибке */
    const DEFAULT_ERROR_MESSAGE = 'common.errorGeneral';

    /** Yml */
    const ERROR = 0;


    /** Ошибка данные не найдены */
    const API_ERROR_NO_CONTENT = 204;

    /** Ошибка: группа ошибок связанных с activemq */
    const ERROR_ACTIVEMQ = 253;


    /** Код ошибки "Устройство с кодом уже существует в магазине" */
    const ERROR_DUPLICATE_CODE = 255;

    /** Ошибка неподдерживаемой версии апи */
    const ERROR_API_INVALID_VERSION = 256;

    /** Код ошибки проксирования process_input */
    const API_ERROR_PROCESS_INPUT = 257;

    /** Превышен лимит авто генерации штрих кода */
    const API_ERROR_LIMIT_AUTO_GENERATE_BARCODE_EXCEEDED = 258;

    /** Ошибка аутентификации пользователя по grant_type=password*/
    const API_AUTHENTICATION_ERROR = 300;
    /** Ошибка запроса время жизни access token истекло*/
    const API_INVALID_ACCESS_TOKEN = 301;
    /** Ошибка аутентификации пользователя по refresh_token*/
    const API_INVALID_REFRESH_TOKEN = 302;
    /** Следующий запрос надо отправлять с подтверждением капчи */
    const API_NEED_SEND_CAPTCHA = 303;
    /** Нет капчи или неверно введена */
    const API_INVALID_CAPTCHA = 304;
    /** Пользователь временно заблокирован по ip */
    const API_USER_IP_BLOCKED = 305;

    /** Пользователь заблокирован в системе i-Retail */
    const API_USER_BLOCKED = 306;

    /** Неизвестная ошибка - для всех эксепшнов, не описанных в коде специально */
    const API_GENERAL_ERROR = 400;

    /** Ошибка авторизации */
    const API_AUTHORIZATION_ERROR = 401;
    /** Ошибка валидации */
    const API_ACCESS_ERROR = 403;

    const API_NOT_FOUND_ERROR = 404;

    /** Ошибка валидации */
    const API_VALIDATION_ERROR = 460;


    /** Дефолтный код для всех эксепшнов, вызваных вручную */
    const HTTP_DEFAULT_CODE = 400;

    /** @var array $dataError Данные ошибки */
    public $dataError;


    /**
     * ApiException constructor.
     * @param int $code
     * @param array $dataError
     * @param null $messageLabel
     */
    public function __construct($code = self::API_GENERAL_ERROR, $messageLabel = null, $dataError = [], $http_code = self::HTTP_DEFAULT_CODE)
    {
        if (isset(Response::$httpStatuses[$code]) && $code != 204) { //204 код какой-то странненький
            $http_code = $code;
        }
        if (is_null($messageLabel)) {
            $messageLabel = self::getErrorMessageByCode($code);
        }

        $this->dataError = $dataError;
        parent::__construct($http_code, \Yii::t('exceptions', $messageLabel), $code);
    }


    /**
     * Возвращает дефолтный текст ошибки по коду
     * @param $code
     * @return string
     */
    public static function getErrorMessageByCode($code)
    {
        $errorMessages = self::getMessagesList();

        if (!isset($errorMessages[$code])) {
            $code = self::ERROR;
        }

        return '' . $errorMessages[$code];
    }

    protected static function getMessagesList()
    {
        return [
            self::ERROR =>
                'error.yml.commonError',
            self::API_GENERAL_ERROR =>
                'Oops something went wrong',
            self::ERROR_ACTIVEMQ =>
                'error.activemq',
            self::API_ACCESS_ERROR =>
                'error.access',
            self::API_VALIDATION_ERROR =>
                'error.validation',
            self::API_AUTHORIZATION_ERROR =>
                'error.authorization',
            self::API_ERROR_NO_CONTENT =>
                'error.not.content',

        ];
    }

    /**
     * Вернуть данные ошибки
     *
     * @return array
     */
    public function getDataError()
    {
        return $this->dataError;
    }

}