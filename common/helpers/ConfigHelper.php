<?php
/**
 * Created by PhpStorm.
 * User: kaiser
 * Date: 13.01.16
 * Time: 16:37
 */

namespace common\helpers;
//use common\services\exceptions\HumanFriendlyException;
use yii\console\Exception;
use yii\helpers\BaseInflector;
use yii\helpers\FileHelper;


/**
 * Хэлпер для массивов, расширяет встроенный юишный
 */
class ConfigHelper extends \yii\helpers\ArrayHelper
{
    public static function getRabbitMqQueues($queue, $param = null)
    {
        $fileName = \Yii::getAlias('@root/server_configs/rabbitmq.json');
        if($configJson = file_get_contents($fileName)){
            $config = json_decode($configJson, true);
            if(isset($config[$queue])){
                if(null !== $param){
                    if(isset($config[$queue][$param])) {
                        return $config[$queue][$param];
                    } else {
                        return $config[$queue];
                    }
                }else{
                    throw new \yii\base\Exception('Не возможно найти параметр queue - ' . $queue . ' param '. $param , 500);
                }
            }else{
                throw new \yii\base\Exception('Не возможно определить очередь - ' . $queue , 500);
            }
        }else{
            throw new \yii\base\Exception('Не возможно прочитать файл - ' . $fileName , 500);
        }
    }

}