<?php
/**
 * Created by PhpStorm.
 * User: ipotupchik
 * Date: 16.03.16
 * Time: 11:09
 */

namespace common\helpers;


use common\services\exceptions\ApiException;
use common\services\exceptions\HumanFriendlyException;
use yii\web\Request;

/**
 * Хэлпер для контроллеров АПИ
 *
 * @package app\behaviors\apiHelpers
 */
class ApiHelper
{
    /**
     * Вытащить целочисленный параметр из запроса. Значение параметра будет приведено к integer.
     * @param Request $request
     * @param string $paramName Название требуемого параметра
     * @param mixed $default значение по умолчанию
     * @return int
     * @throws \Exception Если параметр не является числом
     */
    public static function grabIntParamFromRequest(Request $request, $paramName, $default = null)
    {
        $paramValue = self::grabParamFromRequest($request, $paramName, $default);
        if (!is_numeric($paramValue)) {
            throw new ApiException("error.parameterIsNotInt",
                HumanFriendlyException::API_PARAMETER_IS_NAN, [$paramName]);
        }
        return (int) $paramValue;
    }

    /**
     * Вытащить параметр из запроса.
     * @param Request $request Объект запроса
     * @param string $paramName Название требуемого параметра
     * @param mixed $default значение по умолчанию
     * @return mixed
     * @throws \Exception Если в запросе нет параметра с названием $paramName
     */
    public static function grabParamFromRequest(Request $request, $paramName, $default = null)
    {
        $paramValue = $request->post($paramName, $default);
        if (!isset($paramValue)) {
            throw new ApiException("error.commonExpectedOption",
                HumanFriendlyException::API_PARAMETER_EXPECTED, [$paramName]);
        }
        return $paramValue;
    }

}