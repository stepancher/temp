<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 01.10.16
 * Time: 12:08
 */

namespace common\helpers;


/**
 * Обертка для установки flash уведомлений
 * Class SetFlashHelper
 * @package common\helpers
 */
class SetFlashHelper
{
    public static function set($errors)
    {
        $errorString = '';
        foreach ($errors as $attr => $arrayErrors) {
            $errorString .= '<br>'.implode(',', $arrayErrors);
        }
        if(!empty($errorString)){
            \Yii::$app->session->setFlash('warning', 'Что-то пошло не так'. $errorString);
        }
    }


}