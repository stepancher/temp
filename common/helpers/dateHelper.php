<?php
namespace common\helpers;

class dateHelper
{
    // Возвращает дату в формате: 01 января 2000
    public static function russianDate($dateIn) {
        $date = explode(".", date("d.m.Y", strtotime($dateIn)));
        switch ($date[1]) {
            case 1: $m = 'января'; break;
            case 2: $m = 'февраля'; break;
            case 3: $m = 'марта'; break;
            case 4: $m = 'апреля'; break;
            case 5: $m = 'мая'; break;
            case 6: $m = 'июня'; break;
            case 7: $m = 'июля'; break;
            case 8: $m = 'августа'; break;
            case 9: $m = 'сентября'; break;
            case 10: $m = 'октября'; break;
            case 11: $m = 'ноября'; break;
            case 12: $m = 'декабря'; break;
        }
        return $date[0].'&nbsp;'.$m.'&nbsp;'.$date[2];
    }
}