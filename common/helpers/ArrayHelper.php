<?php
/**
 * Created by PhpStorm.
 * User: kaiser
 * Date: 13.01.16
 * Time: 16:37
 */

namespace common\helpers;
//use common\services\exceptions\HumanFriendlyException;
use yii\helpers\BaseInflector;


/**
 * Хэлпер для массивов, расширяет встроенный юишный
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{

    /**
     * Выбрать из массива $array те элементы, ключи которых есть среди значений массива $keys
     * @param array $array Фильтруемый массив
     * @param array $keys Массив ключей, которые должны остаться в $array
     * @return array Отфильтрованный массив
     */
    public static function filterByKeys($array, $keys)
    {
        $keys = array_flip($keys);
        return array_intersect_key($array, $keys);
    }

    /**
     * В каждом подмассиве массива $arrays выбрать те элементы, ключи которых есть среди значений массива $keys
     * @param array $arrays Массив массивов
     * @param array $keys Нужные ключи
     * @return array Массив, каждый вложенный массив которого имеет ключи только из списка $keys
     */
    public static function mapFilterByKeys($arrays, $keys)
    {
        $keys = array_flip($keys);
        $items = array_map(function($array) use($keys) {
            return array_intersect_key($array, $keys);
        }, $arrays);
        return $items;
    }

    /**
     * Вернуть массив состоящий из элементов с ключами id входнрого массива.
     * Пример:
     *      Входной массив:
     *      [
     *          [
     *              id => 10,
     *              title => тест,
     *          ],
     *          [
     *              id => 15,
     *              title => тест2,
     *          ],
     *      ]
     *      Результат: [10, 15]
     *
     * @param array $array Входной массив
     * @return array Массив из id
     */
    public static function mapId($array)
    {
        return parent::getColumn($array, 'id');
    }

    /**
     * Вернуть массив без определенных ключей
     *
     * Пример:
     *
     * Входной массив
     * $array = [
     *     'id'
     *     'amount'
     *     'user' => [
     *         'id'
     *         'api_key'
     *     ]
     * ]
     *
     * Массив ключей
     * $keys = [
     *     'user' => ['api_key']
     * ]
     *
     * Результирующий массив
     * $result = [
     *     'id'
     *     'amount'
     *     'user' => [
     *         'id'
     *     ]
     * ]
     *
     * @param array $array Массив
     * @param array $keys Ключи, которых не должно быть в итоговом массиве
     * @return array Итоговый массив
     */
    public static function mapUnsetRelation(array $array, array $keys)
    {
        return array_map(function($value) use ($keys) {
            return self::unsetByKeys($value, $keys);
        }, $array);
    }

    /**
     * Преобразования ключей массива в camelCase ключи
     *
     * @param $array
     * @return array
     */
    public static function variablize($array)
    {
        $camelCaseArray = [];
        foreach ($array as $key => $item) {

            if (is_array($item)) {
                $item = self::variablize($item);
            }

            $key = is_numeric($key) ? $key : BaseInflector::variablize($key);
            $camelCaseArray[$key] = $item;
        }

        return $camelCaseArray;
    }

    private static function unsetByKeys($object, array $keys)
    {
        foreach ($keys as $key => $value) {


            if (is_array($value) && isset($object->$key)) {

                self::unsetByKeys($object->$key, $value);
            } else {

                if (isset($object->$value)) {
                    unset($object->$value);
                }
            }
        }

        return $object;
    }

    /**
     * Выполнить замену ключей в массиве сохранив значения
     * @param array $array Массив над которым совершается операция
     * @param array $keys Массив пар значений 'старыйКлюч' => 'новыйКлюч'
     * @param bool $force Если false, то при попытки перезаписать уже существующий элемент массива генерируется \Exception
     * @return array Массив полученный в результате
     * @throws HumanFriendlyException
     */
//    public static function replaceKeys(array $array, array $keys, $force = false)
//    {
//        foreach ($keys as $oldKey => $newKey) {
//            if (array_key_exists($oldKey, $array)) {
//                if (isset($array[$newKey]) && $force == false) {
//                    throw new HumanFriendlyException("error.keyAlreadyExistsInArray",
//                        HumanFriendlyException::ERROR_HELPERS, [$newKey]);
//                }
//                $array[$newKey] = $array[$oldKey];
//                unset($array[$oldKey]);
//            }
//        }
//        return $array;
//    }
}