<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
    ],
];

$config['bootstrap'][] = 'debug';
$config['modules']['debug']['class'] = 'yii\debug\Module';
$config['modules']['debug']['allowedIPs'] = require(__DIR__ . '/debug-panel-ips.php');

return $config;