<?php
return [
    'components' => [
        'assetManager' => [
            'forceCopy'=>false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error.log',
                    'levels' => ['error']
                ]
            ],
        ],
    ],
];
