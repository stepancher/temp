<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/application.log',
                    'levels' => ['error'],
                    'except' => ['centrifugo', 'elasticsearch']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/centrifugo.log',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['centrifugo']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/elasticsearch.log',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['elasticsearch']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/warning.log',
                    'levels' => ['warning']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/info/application.log',
                    'levels' => ['info'],
                    'except' => ['centrifugo', 'elasticsearch']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/info/centrifugo.log',
                    'levels' => ['info'],
                    'logVars' => [],
                    'categories' => ['centrifugo']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/info/elasticsearch.log',
                    'levels' => ['info'],
                    'logVars' => [],
                    'categories' => ['elasticsearch']
                ],
            ],
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['bootstrap'][] = 'extensionDebug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        //добавляемые панели (можно включать, можно нет)
        'panels' => [
            //панель для возможности форматирования sql-запросов в панели дебага
            'db' => 'quartz\debug\panels\DbPanel',
            //панель для дампа на экран и в дебагер
            'dumper' => 'quartz\debug\panels\DumperPanel',
        ]
    ];
    $config['modules']['debug']['allowedIPs'] = ['*'];


    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'model' => 'quartz\gii\generators\model\Generator',
            'crud' => 'quartz\gii\generators\crud\Generator',
        ],
        'controllerNamespace' => 'quartz\gii\controllers',

    ];

    $config['modules']['extensionDebug'] = [
        'class' => 'quartz\debug\Debug',
        //объявление дефолтного типа дампера
        'dumperType' => 'print_r'
    ];

}

return $config;
