<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/application.log',
                    'levels' => ['error'],
                    'except' => ['centrifugo', 'elasticsearch']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/centrifugo.log',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['centrifugo']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/elasticsearch.log',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['elasticsearch']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/warning.log',
                    'levels' => ['warning']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/info/application.log',
                    'levels' => ['info'],
                    'except' => ['centrifugo', 'elasticsearch']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/info/centrifugo.log',
                    'levels' => ['info'],
                    'logVars' => [],
                    'categories' => ['centrifugo']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/info/elasticsearch.log',
                    'levels' => ['info'],
                    'logVars' => [],
                    'categories' => ['elasticsearch']
                ],
            ],
        ],
    ],
];

return $config;
