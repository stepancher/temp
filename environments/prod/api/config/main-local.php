<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/application.log',
                    'levels' => ['error'],
                    'except' => ['centrifugo', 'elasticsearch']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/centrifugo.log',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['centrifugo']
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error/elasticsearch.log',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['elasticsearch']
                ],
            ],
        ],
    ],
];

return $config;