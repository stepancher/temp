<?php
return [
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error.log',
                    'levels' => ['error']
                ],
            ],
        ]
    ]
];
