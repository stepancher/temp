<?php
namespace api\models\user;

use common\models\user\UserProfile;
use quartz\user\models\User;
use vendor\quartz\mailnotify\models;
use Yii;
use yii\base\Model;

/**
 * Форма регистрации
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $id_user_invite;

    public function rules()
    {
        return [
            [['email'], 'filter', 'filter' => 'strtolower'],
            [['username', 'email', 'password'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'password'], 'filter', 'filter' => 'strip_tags'],
            [['username', 'email', 'password'], 'required', 'message' => \Yii::t('auth', 'can\'t be empty')],
            ['username', 'string', 'min' => 2, 'max' => 100, 'tooShort' => \Yii::t('auth', 'minimum character', ['n' => 2]), 'tooLong' => \Yii::t('auth', 'maximum character', ['n' => 255])],

            ['email', 'email', 'message' => \Yii::t('auth', 'incorrect email')],
            [
                'email', 'unique',
                'targetClass' => Yii::$app->getModule('user')->model('User', [])->classname(),
                'targetAttribute' => ['email' => 'lower(email)'],
                'message' => \Yii::t('auth', 'This email address has already been taken.')
            ],

            [['password'], 'string', 'min' => 6, 'tooShort' => \Yii::t('auth', 'minimum character', ['n' => 6])],
        ];
    }

    /**
     * Регистрация пользователя
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            /** @var User $user */
            $user = Yii::$app->getModule('user')->model('User', []);
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setStatus(10); //todo деактивировать статус
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailConfirmToken(); //создаем токен потверждения

            $transaction = Yii::$app->db->beginTransaction(); //короч первое нормальное использование транзакций на проекте! йоу это работает!
            try {
                $user->save();
                $userRole = Yii::$app->authManager->getRole('user'); //устанавливаем дефолтную роль юзера
                Yii::$app->authManager->assign($userRole, $user->id);

                /** @var UserProfile $userProfile */
                $userProfile = Yii::$app->getModule('user')->model('UserProfile', []);
                $userProfile->sex = '1';
                $userProfile->firstname = $this->username;
                $userProfile->id_user = $user->getId();
                $userProfile->save();

                if (!$user->hasErrors() && !$userProfile->hasErrors()) {
                    $transaction->commit();
                    $this->sendEmailConfirm($user); //отправляем подтверждение email
                    \Yii::$app->cache->set('user_last_signup_' . \Yii::$app->request->getUserIP(), time()); //пишем в мемкеш время последней удачной регистрации
                    return $user;
                } else {
                    Yii::error([
                        'Ошибка при регистрации',
                        'userErrors' => $user->errors,
                        'userProfileErrors' => $userProfile->errors,
                    ], 'user.signup');
                    $transaction->rollBack();
                    return false;
                }
            } catch (\Exception $e) {
                Yii::error([
                    'Ошибка при регистрации',
                    'error' => $e->getMessage(),
                ], 'user.signup');
                $transaction->rollBack();
                return false;
            }
        }
        return null;
    }

    /**
     * отправка пользователю письма с подтверждением почты
     * @param $user
     * @return bool
     */
    public static function sendEmailConfirm($user)
    {
        if ($user) {
            return models\MailNotify::send('Basetype', ['user' => $user]);
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('auth', 'username'),
            'email' => \Yii::t('auth', 'email'),
            'password' => \Yii::t('auth', 'password'),
        ];
    }
}
