<?php
namespace api\models\user;

use common\models\user\User;
use common\models\user\UserProfile;
use quartz\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\base\Model;


/**
 * Class UserForm Форма сохдания и редактирования юзера в админке
 * @property string $id
 * @property string $code
 * @property string $username
 * @property string $email
 * @property string $newPassword
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $date_birthday
 * @property string $sex
 *
 *
 * @property string $avatar
 * @property string $skype
 * @property string $phone_number
 *
 * @property string $site
 * @property string $about_me
 * @property string $is_allergy
 * @property string $is_disability
 * @property string $id_language
 * @property string $id_city
 */
class UserForm extends Model
{
    public $id;
    public $code;
    public $newPassword;
    public $email;
    public $firstname;
    public $middlename;
    public $lastname;
    public $date_birthday;
    public $sex;

    public $avatar;
    public $skype;
    public $phone_number;

    public $site;
    public $about_me;
    public $is_allergy;
    public $is_disability;
    public $id_language;
    public $id_city;


    /**
     * @return array массив валидации
     */
    public function rules()
    {
        $rules = [
            [[ 'phone_number', 'skype', 'site', 'about_me', 'firstname', 'middlename', 'lastname', 'avatar'], 'filter', 'filter' => 'trim'],
            [[ 'phone_number', 'skype', 'site', 'about_me', 'firstname', 'middlename', 'lastname', 'avatar'], 'filter', 'filter' => 'strip_tags'],

            [['phone_number', 'firstname', 'middlename', 'lastname'], 'string'],
            [['id_city', 'id_language'], 'integer'],
            [['is_allergy', 'is_disability'], 'boolean'],
//            ['phone_number', 'filter', 'filter' => ['\quartz\user\helpers\phoneHelper', 'erasePhone']],
            ['phone_number', 'unique', 'targetClass' => '\quartz\user\models\UserProfile', 'filter' => function ($query) {
                return $query->andWhere(['not', ['id_user' => $this->id]])->andWhere(['not', ['phone_number' => '']]);
            }, 'message' => \Yii::t('auth', 'Validate.phone_busy')],
            ['code', 'unique', 'targetClass' => 'common\models\user\User', 'filter' => function ($query) {
                return $query->andWhere(['not', ['id' => $this->id]]);
            }, 'message' => \Yii::t('auth', 'Validate.code_busy')],

            ['phone_number', 'quartz\tools\validators\MaskValidator', 'mask' => '+XXXXXXXXXXX', 'message' => Yii::t('auth', 'Input format') . ' +XXXXXXXXXXX'],
            ['date_birthday', 'quartz\tools\validators\MaskValidator', 'mask' => 'XX.XX.XXXX', 'message' => Yii::t('auth', 'Input format') . ' XX.XX.XXXX'],

            [['newPassword'], 'string', 'min' => 6, 'tooShort' => \Yii::t('auth', 'minimum character', ['n' => 6])],
            [['date_birthday'], 'safe'],
            ['avatar', 'filter', 'filter' => function ($str) {
                return preg_match('/^[0-9a-f]+\.(png|jpg|jpeg|svg|gif)$/', $str) ? $str : null;
            }],

            [['skype', 'site', 'about_me'], 'string', 'max' => 255, 'tooLong' => \Yii::t('auth', 'maximum character', ['n' => 255])],
            [['sex'], 'number', 'max' => 1, 'min' => 0, 'integerOnly' => true],
        ];

        return $rules;
    }

    /**
     * Изменение пользователя
     * @return true/false saved 2 model User and UserProfile
     */
    public function update()
    {
        $module = Yii::$app->getModule('user')->model('User', []);
        $transaction = Yii::$app->db->beginTransaction(); //короч первое нормальное использование транзакций на проекте! йоу это работает!
        try {
            /**  @var $user User */
            $user = $module::findOne($this->id);
            $user->username = $this->lastname . ' ' . $this->firstname;
            $user->setAttributes($this->getAttributes(['id_city', 'id_language']));
            $user->code = $this->code;

            if ($this->newPassword) {
                $user->setPassword($this->newPassword);
            }
            $user->generateAuthKey();
            $user->setActiveStatus();

            if ($user->save()) {
                /** Юзера сохранили сохраняем профиль*/
                $module = Yii::$app->getModule('user')->model('UserProfile', []);
                /**  @var $userProfile UserProfile */
                $userProfile = $module::findOne($this->id);
                $userProfile->attributes = $this->attributes;
                if ($userProfile->save()) {
                    $transaction->commit();
                    return true;
                } else {
                    $transaction->rollBack();
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Exception $e) {
            Yii::error([
                'Ошибка при обновлении пользователя',
                'error' => $e->getMessage(),
            ], 'user.signup');
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * Массив лэйблов локализованный
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'code' => \Yii::t('auth', '#ID'),
            'password' => \Yii::t('auth', 'password'),
            'date_birthday' => Yii::t('auth', 'Date Birthday'),
            'sex' => Yii::t('auth', 'Sex'),
            'avatar' => Yii::t('auth', 'Avatar'),
            'skype' => Yii::t('auth', 'Skype'),
            'phone_number' => Yii::t('auth', 'Phone number'),
            'newPassword' => Yii::t('auth', 'New password'),
        ];
    }
}
