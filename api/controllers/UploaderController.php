<?php
namespace api\controllers;

/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 04.11.16
 * Time: 17:45
 */

use common\services\UploadService;
use yii\web\Controller;

/**
 * Контроллер для работы с изображениями
 * Class UploaderController
 * @package api\controllers
 */
class UploaderController extends Controller
{
    /**
     * Загрузка изображения
     * @post file multipart/form-data файл изображения только png, jpeg, jpg, gif
     * @return array
     * {
     *      "status": "OK",
     *      "message": "Изображение успешно сохранено"
     *      "name": "57dcb7398384f.png"
     * }
     */
    public function actionSaveImage()
	{
        return UploadService::saveImage();
	}
}
