<?php

/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 11:29
 */

namespace api\controllers;

use common\exceptions\ApiException;
use common\models\user\User;
use common\services\auth\AuthService;
use common\services\CategoryService;
use common\services\user\UserService;
use yii\rest\ViewAction;
use yii\web\Controller;

/**
 * API для работы с пользователями
 *
 * @package app\controllers
 */
class UserController extends Controller
{
    /**
     * Общие данные пользователя
     * @post code идешник пользователя, если пусто возвращает текущего авторизованного пользователя
     * @return array
     * {
     *  "user": {
     *      "code": "3325253306",
     *      "email": "s.cherepanov@freematiq.com",
     *      "username": "Степан Черепанов",
     *      "id_city": null,
     *      "id_language": null
     *  },
     * "favoriteCategories": [
     *      1,
     *      2,
     *      3
     *  ]
     * }
     * @throws ApiException 204 контент не найден
     */
    public function actionGetData()
    {
        $code = \Yii::$app->request->post('code', null);
        if ($code === null) {
            $user = \Yii::$app->user->identity;
        } else {
            $user = User::findOneByCode($code);
        }
        if (is_null($user)) {
            throw new ApiException(ApiException::API_ERROR_NO_CONTENT);
        }

        return [
            'user' => UserService::getShortData($user),
            'favoriteCategories' => CategoryService::getCategoryByUser($user)
        ];
    }

    /**
     * Расширенные данные пользователя. Профиль
     * @post code идешник пользователя, если пусто возвращает текущего авторизованного пользователя,
     * @return
     * {
     * "userProfile": {
     *      "firstname": "Степан Черепанов",
     *      "middlename": "",
     *      "lastname": "",
     *      "date_birthday": null,
     *      "site": null,
     *      "about_me": null,
     *      "is_allergy": false,
     *      "is_disability": false,
     *      "sex": true,
     *      "avatar": "/upload/default/no-avatar-male.gif"
     *  }
     * }
     */
    public function actionGetProfile()
    {
        $code = \Yii::$app->request->post('code', null);
        if ($code === null) {
            $user = \Yii::$app->user->identity;
        } else {
            $user = User::findOneByCode($code);
        }
        if (is_null($user)) {
            throw new ApiException(ApiException::API_ERROR_NO_CONTENT);
        }

        return [
            'userProfile' => $user->profile->getShortData()
        ];
    }

    /**
     * Авторизует пользователя по почте и паролю
     * @post email Email пользователя (Email)
     * @post password Пароль пользователя
     * @return array
     * {
     *  "user": {
     *      "code": "3325253306",
     *      "email": "s.cherepanov@freematiq.com",
     *      "username": "Степан Черепанов",
     *      "id_city": null,
     *      "id_language": null
     *  },
     * "favoriteCategories": [
     *      1,
     *      2,
     *      3
     *  ]
     * }
     * @throws ApiException 400 общая ошибка
     * @throws ApiException 460 ошибка валидации
     */
    public function actionSignin()
    {
        return (new AuthService())->signin(\Yii::$app->request, \Yii::$app->user);
    }

    /**
     * Регистрирует пользователя по логину, мылу и паролю
     * @post username Логин пользователя
     * @post email Email ользователя
     * @post password Пароль пользователя
     * @return array
     * {
     *  "user": {
     *      "code": 1597638400,
     *      "email": "s.cherepanov@freematiq.com",
     *      "username": "Степан Черепанов",
     *      "id_city": null,
     *      "id_language": null
     *  },
     * "favoriteCategories": [
     *      1,
     *      2,
     *      3
     *  ]
     * }
     * @throws ApiException 400 общая ошибка
     * @throws ApiException 460 ошибка валидации
     */
    public function actionSignup()
    {
        return (new AuthService())->signup(\Yii::$app->request, \Yii::$app->user);
    }

    /**
     * Разлогинить пользователя
     * @return array
     * {
     *  "status": "OK",
     *  "message": "You have successfully logged out"
     * }
     */
    public function actionLogout()
    {
        return (new AuthService())->logout(\Yii::$app->user);
    }

}
