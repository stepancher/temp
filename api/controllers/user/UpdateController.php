<?php

/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 11:29
 */

namespace api\controllers\user;

use api\controllers\ApiController;
use common\exceptions\ApiException;
use common\services\user\UserService;

/**
 * API для работы с пользователями
 *
 * @package app\controllers
 */
class UpdateController extends ApiController
{
    /**
     * Редактирование пользователя
     * @post code string ID пользователя
     * @post lastname Фамилия пользователя
     * @post firstname Имя пользователя
     * @post middlename Отчество пользователя
     * @post newPassword Пароль пользователя
     * @post date_birthday Дата рождения xx.xx.xxxx
     * @post skype Скайп
     * @post phone_number Номер телефона +xxxxxxxxxxx
     * @post sex 0 женский 1 мужской
     * @post site Сайт пользователя
     * @post id_city Идентификатор города
     * @post id_language  Идентификатор языка
     * @post is_allergy  Аллергия boolean
     * @post is_disability  Инвалидность boolean
     * @post about_me  О себе
     * @post avatar Имя файла, которое вернул uploader
     * @see "/user/update/personal"
     * @return array
     * {
     *      "status": "OK",
     *      "message": "Данные пользователя успешно сохранены"
     * }
     * @throws ApiException 400 общая ошибка
     * @throws ApiException 401 ошибка авторизации
     * @throws ApiException 460 ошибка валидации
     */
    public function actionPersonal()
    {
        return (new UserService())->updatePersonal(\Yii::$app->request, \Yii::$app->user);
    }
}
