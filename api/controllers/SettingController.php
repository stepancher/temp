<?php

/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 11:29
 */

namespace api\controllers;

use common\services\SettingService;

/**
 * API для работы с Настройками сайта
 *
 * @package app\controllers
 */
class SettingController extends ApiController
{
    /**
     * Настройка сайта по коду
     * @post code string строковый код настройки на латинице, если не указан вернется весь массив настроек
     * @return array
     * {
     *  "setting": {
     *      "value": "Tetrus.club",
     *      "code": "site_name"
     *  }
     * }
     */
    public function actionGetByCode()
    {
        return SettingService::getSetting(\Yii::$app->request->post('code', null));
    }

    /**
     * Все настройки сайта
     * @return array
     *  {
     *      "settings": [
     *          {
     *              "value": "Tetrus.club",
     *              "code": "site_name"
     *          },
     *          {
     *              "value": null,
     *              "code": "site_logo"
     *          },
     *  ...
     *      ]
     * }
     */
    public function actionGetAll()
    {
        return SettingService::getAllSettings();
    }
}
