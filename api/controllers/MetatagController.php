<?php

/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 11:29
 */

namespace api\controllers;

use common\exceptions\ApiException;
use common\services\MetatagService;

/**
 * API для работы с Метатегами
 *
 * @package app\controllers
 */
class MetatagController extends ApiController
{
    /**
     * Метатеги текущей страницы сайта, опеределяет по referer адресу
     * @return array
     * {
     *  "metatag": {
     *      "title": "Tetrus.club - карта мероприятий",
     *      "keywords": "Tetrus, тетрус, карта, мероприятий, сообщества, места",
     *      "description": "Tetrus.club - карта городских соообществ",
     *      "others": "Другие мета теги, хз чего это такое",
     *      "url": "/"
     *  }
     * }
     * @throws ApiException 204 not found metadata
     */
    public function actionGetCurrent()
    {
        $url = \Yii::$app->request->referrer;
        return MetatagService::getMetaTagByUrl($url);
    }

    /**
     * Метатеги страницы сайта по url
     * @post url string адрес страницы от корня домена /
     * @return array
     * {
     *  "metatag": {
     *      "title": "Tetrus.club - карта мероприятий",
     *      "keywords": "Tetrus, тетрус, карта, мероприятий, сообщества, места",
     *      "description": "Tetrus.club - карта городских соообществ",
     *      "others": "Другие мета теги, хз чего это такое",
     *      "url": "/"
     *  }
     * }
     * @throws ApiException 204 not found metadata
     */
    public function actionGetByUrl()
    {
        return MetatagService::getMetaTagByUrl(\Yii::$app->request->post('url', null));
    }
}
