<?php

/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 11:29
 */

namespace api\controllers;

use common\exceptions\ApiException;
use common\services\CategoryService;

/**
 * API для работы с Настройками
 *
 * @package app\controllers
 */
class CategoryController extends ApiController
{
    /**
     * Все категории сайта
     * @return array
     * {
     *      "categories" : [
     *          {
     *              "id": 1,
     *              "name": "Неформальные"
     *          },
     *          {
     *              "id": 2,
     *              "name": "Знакомства"
     *          },
     *          ...
     *      ]
     * }
     */
    public function actionGetAll()
    {
        return ['categories' => CategoryService::getAllToList()];
    }

    /**
    * категория сайта по id
    * @post id идентификатор категории
    * @return array
    * {
    *   "category": {
    *       "id": 1,
    *       "name": "Неформальные"
    *   }
    * }
    */
    public function actionGetById()
    {
        return ['category' => CategoryService::getCategoryData(\Yii::$app->request->post('id', null))];
    }
}
