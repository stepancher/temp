<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 11.09.16
 * Time: 15:50
 */

namespace api\controllers;

use api\filters\ApiFormatFilter;
use yii\base\Controller;

/**
 * Базовый контроллер для API контроллеров
 *
 * @package app\controllers
 */
class ApiController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'formatter' =>  [
                'class' => ApiFormatFilter::className(),
            ],
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                    'application/xml' => \yii\web\Response::FORMAT_XML,
                ],
                'languages' =>[
                    'ru-RU',
                    'en-EN'
                ]
            ],
        ];
    }
}
