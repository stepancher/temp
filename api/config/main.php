<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'tetrus-club-api',
    'name' => 'tetrus.club',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'homeUrl' => '/api',
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/routes.php'),
        ],
        'request' => [
            'baseUrl' => '/api',
            'enableCookieValidation' => true,
            'enableCsrfValidation' => false, //todo вернуть
            'enableCsrfCookie' => true,
            'cookieValidationKey' => 'wldkjvqwpivbQELJQWv3n9WEQF',
            'csrfParam' => '_csrf_api',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'text/xml' => 'light\yii2\XmlParser',
                'application/xml' => 'light\yii2\XmlParser',
            ],
        ],
        'response' => [
            'format' => \yii\web\Response::FORMAT_JSON //дефолтный формат
        ],
        'errorHandler' => [
            'class' => 'api\components\ErrorHandler',
        ],
    ],
    'modules' => [
        'user' => [
            'controllerMap' => [
                'user' => 'api\controllers\UserController',
                'update' => 'api\controllers\user\UpdateController'
            ],
        ],
        'metatag' => [
            'controllerMap' => [
                'metatag' => 'api\controllers\MetatagController',
            ],
        ]
    ],
    'params' => $params,
];