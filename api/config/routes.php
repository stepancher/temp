<?php
return [
    'user' => 'user/user/index',
    'user/update/personal' => 'user/update/personal',
    'user/update/notify' => 'user/update/notify',

    'user/signin' => 'user/user/signin',
    'user/signup' => 'user/user/signup',
    'user/logout' => 'user/user/logout',

    'user/get-profile' => 'user/user/get-profile',
    'user/get-data' => 'user/user/get-data',

    'uploader/fileapi-upload' => 'uploader/fileapi-upload',


    'setting/get-all' => 'setting/get-all',
    'setting/get-by-code' => 'setting/get-by-code',

    'category/get-all' => 'category/get-all',
    'category/get-by-id' => 'category/get-by-id',

    'metatag/get-current' => 'metatag/metatag/get-current',
    'metatag/get-by-url' => 'metatag/metatag/get-by-url',
];
