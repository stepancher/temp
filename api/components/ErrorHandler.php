<?php
/**
 * Created by PhpStorm.
 * User: stepancher
 * Date: 10.09.16
 * Time: 16:38
 */

namespace api\components;


use CentralDesktop\Stomp\Exception;
use common\exceptions\ApiException;
use yii\helpers\VarDumper;
use yii\web\ErrorHandler as BaseErrorHandler;

class ErrorHandler extends BaseErrorHandler
{

    public function handleException($exception)
    {

        $this->exception = $exception;

        // disable error capturing to avoid recursive errors while handling exceptions
        $this->unregister();

        // короч в cli нет кодов 400, 404 и тп
        if (PHP_SAPI !== 'cli') {
            http_response_code(500);
        }


        /** Логирование ощибки */
        $this->logException($exception);
        if ($this->discardExistingOutput) {
            $this->clearOutput();
        }

        /** Наши ошибки */
        if ($exception instanceof ApiException) {
            $responseData = [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ];
            if ($items = $exception->getDataError()) {
                $responseData['items'] = $items;
            }
        } else {
            /** ошибки Yii и пр */
            $responseData = [
                'code' => $exception->getCode() == 0 ? 400 : $exception->getCode(),
                'message' => YII_DEBUG ? $exception->getMessage() : \Yii::t('exceptions', 'Oops something went wrong'),
            ];
        }

        /** В дебаг режиме расширяем ошибку за счет Trace */
        if (YII_DEBUG) {
            $responseData['trace'] = $exception->getTraceAsString();
        }

        \Yii::$app->response->data = $responseData;

        /** @var integer $statusCode исключаем 200 код*/
        $statusCode = $exception->statusCode == 200 || $exception->statusCode == null ? 400 : $exception->statusCode;
        \Yii::$app->response->setStatusCode($statusCode);

        \Yii::$app->response->send();

        \Yii::getLogger()->flush(true);

        $this->exception = null;
    }
}