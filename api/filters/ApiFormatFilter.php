<?php
/**
 * Created by PhpStorm.
 * User: kaiser
 * Date: 27.04.15
 * Time: 10:07
 */

namespace api\filters;


use Yii;
use yii\filters\Cors;
use yii\web\Response;


/**
 * Фильтр для API методов
 *
 * Class ApiFormatFilter
 * @package app\filters
 */
class ApiFormatFilter extends Cors
{

    public function beforeAction($action)
    {
        // Для запроса HTTP OPTIONS используем родительский фильтр Cross Origin Resource Sharing
        // и больше ничего не возвращаем
        if (Yii::$app->getRequest()->getIsOptions()) {
            parent::beforeAction($action);
            return false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Вернуть Json формата:
     * {
     *  "status": true
     *  "result": $result
     * }
     *
     * @param \yii\base\Action $action Объект экшена, после которого выполняется фильтр
     * @param mixed $result рузультат Ответ API метода
     * @return array
     */
    public function afterAction($action, $result)
    {
        \Yii::$app->response->headers->set('Access-Control-Allow-Origin', $this->getAccessControlAllowOrigin());
        \Yii::$app->response->headers->set('Access-Control-Allow-Credentials', 'true');

        /** возвращаем тело ответа */
        return $result;
    }

    /**
     * @return array|mixed|string
     */
    private function getAccessControlAllowOrigin()
    {
        $originHeader = Yii::$app->getRequest()->headers->get('origin');
        $header = \Yii::$app->params['accessControlAllowOrigin'];
        return in_array($originHeader, $header) ? $originHeader : array_shift($header);
    }
}