<?php
/**
 * Created by PhpStorm.
 * User: kaiser
 * Date: 27.04.15
 * Time: 10:07
 */

namespace api\filters;


use app\controllers\ApiController;
use app\helpers\Format;
use app\models\user\Profile;
use app\services\common\auth\AuthService;
use app\models\user\User;
use yii\base\ActionFilter;
use \Yii;

/**
 * Фильтр аутентификации
 *
 * Как работает аутентифиакция в iРознице:
 * -    для клиента (например браузерное веб приложение, мобильное приложение, серверное) создается запись в базе
 *      ПИПО в таблице public.oauth_clients (покачто руками, интерфейса нет), в которой прописывается client_id и
 *      client_secret, которые нужно передавать в каждом запросе к iРознице.
 * -    пользователь должен получить валидный access token. Для веб-приложения сейчас это делается с помощью
 *      angular-сервиса oauth, который является оберткой к сервису ajax. При аутентификации на сайте сервис oauth
 *      получит access token, сохранит его в local storage браузера и будет передавать в каждом запросе, который будет
 *      сделан через него.
 * -    при поступлении запроса к iРознице, если для экшена прописан AuthClientFilter, то фильтр сделает запрос к ПИПО,
 *      что проверить валидность access токена, client_id и client_secret. Если аутентификация прошла успешно, то
 *      будет выполен экшен в котром аутентифицированный пользователь будет доступен через $this->user.
 *      Если аутентификация была не пройдена, то будет выброшено исключение и экшен не будет выполнен.
 *
 * Class AuthClientFilter
 * @package app\filters
 */
class AuthClientFilter extends ActionFilter
{

    /* @var User $user Объект пользователя, соответствующего переданному токену */
    public $user;

    /**
     * До выполения экшена проверить валидность переданных в запросе идентификаторов и токена.
     *
     * Если запрос к ПИПО вернул положительный результат, то будет создан  объект пользователя,
     * соответствующего переданному токену. Этот объект будет доступен в контроллере через $this->user
     *
     * @post client_id ID клиента (приложения)
     * @post client_secret Секретный ключ клиента (приложения)
     * @post access_token Access token пользователя
     *
     * @param \yii\base\Action $action Смотри документацию родителя
     * @return bool Смотри документацию родителя
     * @throws \Exception Если аутентификация не пройдена
     */
//    public function beforeAction($action)
//    {
//        $category = $this->getCategory($action);
//        \Yii::info('', $category);
//        \Yii::info('Запрос к ' . $action->controller->route, $category);
//        $data = [
//            'client_id' => Yii::$app->request->post('client_id'),
//            'client_secret' => Yii::$app->request->post('client_secret'),
//            'access_token' => Yii::$app->request->post('access_token'),
//        ];
//        \Yii::info('Данные из запроса:', $category);
//        \Yii::info(Format::logArrayToString(Yii::$app->request->post()), $category);
//
//        try {
//            $response = ( new AuthService($category) )->checkAccessTokenCached($data);
//
//            \Yii::info('Вернулся ответ от /oauth2/default/authentication: ', $category);
//            \Yii::info(Format::logArrayToString($response), $category);
//            \Yii::info('Аутентификация успешно пройдена', $category);
//
//            $this->user = ( new AuthService )->syncUser($response);
//
//            \Yii::info('Пользователь: ' . Format::logUserInfo($this->user), $category);
//
//            Yii::$app->user->setIdentity($this->user);
//
//            AuthService::checkBlocked();
//
//            Yii::$app->user->currentProfile = Profile::findByAccessToken($data['access_token']);
//
//            return true;
//        } catch(\Exception $e) {
//            // TODO в релизной сборке ios забыли параметры передать
//            if ($action->controller->route === 'payment-in/send-inpas-callback') {
//
//                return true;
//
//            } else {
//                throw $e;
//            }
//        }
//    }

    /**
     * Получить категория для логов
     *
     * @param \yii\base\Action $action Смотри документацию родителя
     * @return string
     */
//    private function getCategory($action)
//    {
//        /** @var ApiController $controller */
//        $controller = $this->owner;
//        return $controller->isLogging ? self::className() : $action->controller->route;
//    }
}