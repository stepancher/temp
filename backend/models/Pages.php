<?php

namespace backend\models;

use Yii;
use quartz\rollback\behaviors\RollbackBehavior;


class Pages extends \quartz\pages\models\Pages
{
    public function behaviors()
    {
        $behaviors = [
            'rollback' => [
                'class' => RollbackBehavior::className(),
            ]
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }
}
