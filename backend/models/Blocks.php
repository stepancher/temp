<?php

namespace backend\models;

use Yii;
use quartz\rollback\behaviors\RollbackBehavior;


class Blocks extends \quartz\blocks\models\Blocks
{
    public function behaviors()
    {
        $behaviors = [
            'rollback' => [
                'class' => RollbackBehavior::className(),
            ]
        ];

        return array_merge(parent::behaviors(), $behaviors);
    }
}
