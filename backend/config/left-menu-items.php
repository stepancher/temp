<?php
return [
    [
        'permission' => 'r_user',
        'activeRoutes' => ['user/admin/index', 'user/admin/create', 'user/admin/update', 'permit/access/role', 'permit/access/update-role', 'permit/access/permission', 'permit/access/update-permission'],
        'route' => '#',
        'content' => '<i class="glyphicon glyphicon-user"></i><span>Пользователи и права</span><i class="fa fa-angle-left pull-right"></i>',
        'subItems' => [
            [
                'activeRoutes' => ['user/admin/index', 'user/admin/create', 'user/admin/update'],
                'route' => '/user',
                'content' => '<i class="fa fa-caret-right"></i> <span>Пользователи</span>',
            ],
            [
                'activeRoutes' => ['permit/access/role', 'permit/access/update-role'],
                'route' => '/permit/access/role',
                'content' => '<i class="fa fa-caret-right"></i> <span>Роли</span>',
            ],
            [
                'activeRoutes' => ['permit/access/permission', 'permit/access/update-permission'],
                'route' => '/permit/access/permission',
                'content' => '<i class="fa fa-caret-right"></i> <span>Права</span>',
            ],
        ]
    ],
    [
        'permission' => 'r_point',
        'activeRoutes' => ['points/event', 'points/place', 'points/community'],
        'route' => '#',
        'content' => '<i class="fa fa-map-marker"></i><span>Точки</span><i class="fa fa-angle-left pull-right"></i>',
        'subItems' => [
            [
                'activeRoutes' => ['points/event/index', 'points/event/create', 'points/event/update', 'points/event/view'],
                'route' => '/points/event',
                'content' => '<i class="fa fa-caret-right"></i> <span>Мероприятия</span>',
            ],
            [
                'activeRoutes' => ['points/community/index', 'points/community/create', 'points/community/update', 'points/community/view'],
                'route' => '/points/community',
                'content' => '<i class="fa fa-caret-right"></i> <span>Сообщества</span>',
            ],
            [
                'activeRoutes' => ['points/place/index', 'points/place/create', 'points/place/update', 'points/place/view'],
                'route' => '/points/place',
                'content' => '<i class="fa fa-caret-right"></i> <span>Места</span>',
            ],
        ]
    ],
    [
        'permission' => 'r_menu',
        'activeRoutes' => ['menu/admin/index', 'menu/admin/edit'],
        'route' => '/menu',
        'content' => '<i class="fa fa-code-fork fa-flip-vertical"></i> <span>Меню</span>',
    ],
    [
        'permission' => 'r_pages',
        'activeRoutes' => ['pages/admin/index', 'pages/admin/edit'],
        'route' => '/pages',
        'content' => '<i class="fa fa-file"></i> <span>Страницы</span>',
    ],
    [
        'permission' => 'r_forms',
        'activeRoutes' => ['forms/admin/index', 'forms/admin', 'forms/admin/create', 'forms/admin/update', 'forms/admin/view', 'forms/admin/show'],
        'route' => '/forms',
        'content' => '<i class="fa fa-table"></i> <span>Формы</span>',
    ],
    [
        'permission' => 'r_blocks',
        'activeRoutes' => ['blocks/admin/index', 'blocks/admin', 'blocks/admin/create', 'blocks/admin/update', 'blocks/admin/view'],
        'route' => '/blocks',
        'content' => '<i class="fa fa-puzzle-piece"></i> <span>Блоки</span>',
    ],
    [
        'permission' => 'r_mailnotify',
        'activeRoutes' => [
            'mailnotify',
            'mailnotify/default/edit',
            'mailnotify/default/type_list',
            'mailnotify/default/type_edit',
            'mailnotify/default/vars_list',
            'mailnotify/default/vars_edit',
    ],
        'route' => '#',
        'content' => '<i class="fa fa-at"></i> <span>Почтовые шаблоны</span> <i class="fa fa-angle-left pull-right"></i>',
        'subItems' => [
            [
                'activeRoutes' => ['mailnotify'],
                'route' => '/mailnotify',
                'content' => '<i class="fa caret-right"></i><span>Шаблоны</span>',
            ],
            [
                'activeRoutes' => ['mailnotify/default/type_list', 'mailnotify/default/type_edit'],
                'route' => '/mailnotify/type_list',
                'content' => '<i class="fa caret-right"></i><span>Типы</span>',
            ],
            [
                'activeRoutes' => ['mailnotify/default/vars_list', 'mailnotify/default/vars_edit'],
                'route' => '/mailnotify/vars_list',
                'content' => '<i class="fa caret-right"></i><span>Переменные</span>',
            ],
        ]
    ],
    [
    'permission' => 'r_metatag',
        'activeRoutes' => ['metatag/index', 'metatag', 'metatag/update'],
        'route' => '/metatag',
        'content' => '<i class="fa fa-code"></i><span>Мета-теги</span>',
    ],
    [
        'permission' => 'r_settings',
        'activeRoutes' => ['settings', 'settings/update'],
        'route' => '/settings',
        'content' => '<i class="fa fa-cogs"></i><span>Настройки</span>',
    ],
    [
        'permission' => 'r_localization',
        'activeRoutes' => ['localization/admin', 'localization/admin/index', 'localization/admin/create', 'localization/admin/update', 'localization/admin/view'],
        'route' => '/localization',
        'content' => '<i class="fa fa-language"></i><span>Локализация</span>',
    ],
    [
        'permission' => 'r_servertest',
        'activeRoutes' => ['servertest/admin', 'servertest/admin/index'],
        'route' => '/servertest',
        'content' => '<i class="fa fa-language"></i><span>Тестирование окружения</span>',
    ],
    [
        'permission' => 'r_category',
        'activeRoutes' => ['category/index', 'category', 'category/update', 'category/create'],
        'route' => '/category',
        'content' => '<i class="fa fa-folder"></i><span>Категории</span>',
    ],
];
