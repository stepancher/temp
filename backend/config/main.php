<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'tetrus-club-backend',
    'name' => 'tetrus.club',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'homeUrl' => '/admin',
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views/layouts' => [
                        '@backend/views/layouts',
                        '@vendor/quartz/yii2-adminlte-theme/views/layouts',
                    ],
                ],
            ],
            'params' => [
                'left-menu-items' => __DIR__ . '/left-menu-items.php',
                'cacheMenu' => true
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => require(__DIR__ . '/routes.php'),
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'quartz\adminlteTheme\assets\AdminLteAsset' => [
                    'skin' => 'skin-red-light',
                ],
            ],
            'forceCopy' => true
        ],
        'request' => [
            'baseUrl' => '/admin',
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'enableCsrfCookie' => true,
            'cookieValidationKey' => 'bxxxxxxx',
            'csrfParam' => '_csrf_backend'
        ],
//        'log' => [
//            'traceLevel' => YII_DEBUG ? 3 : 0,
//            'targets' => [
//                [
//                    'class' => 'yii\log\FileTarget',
//                    'levels' => ['error', 'warning'],
//                ],
//            ],
//        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'modules' => [
        'permit' => [
            'class' => 'quartz\rbac\RbacControlModule',
            'externalMessageSource' => true,
        ],
        'servertest' => [
            'class' => 'quartz\servertest\ServerTest',
            //переманная очередей для ActiveMQ
            'queues' => [
                'base.mail'
            ],
            // Чтобы функция сбора статистки supervisor работала, необходимо подключить
            //расширение php-xmlrpc
            //переменная для процессов supervisord
            'groupname' => [
                'base_amq_send-mail-listener'
            ],
            'externalMessageSource' => true,
            'seoFilesList' => [
                '@backend/web/robots.txt'
            ]
        ],
        'adminlteTheme' => [
            'class' => 'quartz\adminlteTheme\AdminlteTheme',
            'checkFavorite' => true,
        ],
        'rollback' => [
            'class' => 'quartz\rollback\Rollback',
            'maxRollback' => 10,
            'externalMessageSource' => true
        ],
        'dynagrid' => [
            'class' =>'\kartik\dynagrid\Module',
            'dbSettings' => [
                'tableName' => 'dynagrid',
                'filterAttr' => 'filter_id',
                'sortAttr' => 'sort_id'
            ],
            'dbSettingsDtl' => [
                'tableName' => 'dynagrid_dtl'
            ]
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],

    ],
    'as beforeRequest' => [
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'actions' => ['login', 'error'],
                'allow' => true,
            ],
            [
                'allow' => true,
                'actions' => ['logout'],
                'roles' => ['@'],
            ],
            [
                'allow' => true,
                'roles' => ['backend'],
            ],
        ],
    ],
    'params' => $params,
];