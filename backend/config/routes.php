<?php
return [

    'mailnotify/<action>' => 'mailnotify/default/<action>',

    'blocks/<action>' => 'blocks/admin/<action>',
    'blocks' => 'blocks/admin/index',

    'forms/<action>' => 'forms/admin/<action>',
    'forms' => 'forms/admin/index',

    'pages/<action>' => 'pages/admin/<action>',
    'pages' => 'pages/admin/index',

    'permit/<action>' => 'permit/access/<action>',
    'permit' => 'permit/access/index',

    'menu/<action>' => 'menu/admin/<action>',
    'menu' => 'menu/admin/index',

    'logout' => 'user/admin/logout',
    'auth' => 'user/admin/login',

    'user/<action>' => 'user/admin/<action>',
    'user' => 'user/admin/index',

    'redirect/<action>' => 'redirect/admin/<action>',
    'redirect' => 'redirect/admin/index',

    'tasks/<action>' => 'tasks/admin/<action>',
    'tasks' => 'tasks/admin/index',

    'clear-cache' => 'site/clear-cache',
    'plug' => 'site/plug',

    'localization/' => 'localization/admin/',
    'localization' => 'localization/admin/index',

    'servertest/' => 'servertest/admin/',
    'servertest' => 'servertest/admin/index',

    'add-favorite' => 'adminlteTheme/adminlte-theme/add-favorite',
    'delete-favorite' => 'adminlteTheme/adminlte-theme/delete-favorite',

    '/gii/table-columns' => 'gii/default/table-columns',
    '/gii/image-columns' => 'gii/default/image-columns',

    'metatag' => 'metatag/default/index',
    'metatag/<action>' => 'metatag/default/<action>',

    'category' => 'category/index',
    'category/<action>' => 'category/<action>'
];

