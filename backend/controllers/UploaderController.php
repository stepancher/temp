<?php
namespace backend\controllers;

use quartz\multipleUpload\actions\MultipleDeleteAction;
use quartz\multipleUpload\actions\MultipleUploadAction;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use quartz\fileapi\actions\UploadAction as FileAPIUpload;

class UploaderController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'fileapi-upload',
                            'multiple-upload',
                            'multiple-upload-file',
                            'multiple-delete',
                            'fileapi-upload-document',
                        ],
                        'allow' => true,
                        'roles' => ['upload'],
                    ]
                ],
            ],
        ];
    }

    public function actions()
	{
		return [
            'multiple-upload' => [
                'class' => MultipleUploadAction::className(),
            ],
            'multiple-upload-file' => [
                'class' => MultipleUploadAction::className(),
            ],
            'multiple-delete' => [
                'class' => MultipleDeleteAction::className(),
            ],

            'fileapi-upload-document' => [
                'class' => FileAPIUpload::className(),
                'path' => \Yii::$app->params['uploadTempDir'],
                'uploadOnlyImage' => false,
                'validatorOptions' => [
                    'extensions' => 'pdf,xdoc,doc',
                    'maxSize' => 5242880 /* 5мб */,
                    'checkExtensionByMimeType' => false
                ]
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => \Yii::$app->params['uploadTempDir'],
                'validatorOptions' => ['extensions' => 'png, jpeg, jpg, gif', 'checkExtensionByMimeType' => false]
            ],
        ];
	}
}
