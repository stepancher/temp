<?php
/**
 * Created by PhpStorm.
 * User: phantom
 * Date: 17.03.15
 * Time: 13:58
 */

namespace backend\assets;

use yii\web\AssetBundle;


class BowerAsset extends AssetBundle
{
	public $sourcePath = '@bower';

	public $js = [
		'bootstrap-tabdrop/src/js/bootstrap-tabdrop.js',
	];

	public $css = [
	];
}