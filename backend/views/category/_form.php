<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\category\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-6">
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'is_show')->checkbox() ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'is_default')->checkbox() ?>
            </div>
        </div>

    </div>
    <div class="box-footer text-left">
        <?= Html::submitButton(\Yii::t('category', 'Category.button_save'), ['class' => 'btn btn-success', 'name' => 'save-button']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>