<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\category\Category */

$this->title = Yii::t('category', 'Update Category') . ' "' . $model->name . '"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('category', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('category', 'Update');
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

