<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
/* @var $this yii\web\View */
/* @var $searchModel common\models\category\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('category', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn', 'order' => DynaGrid::ORDER_FIX_LEFT],
    'id',
    'name',
    [
        'attribute' => 'is_show',
        'format' => 'raw',
//        'header' => 'Отображать на сайте',
        'content' => function ($data) {
            switch ($data->is_show) {
                case true:
                    return '<span class="user-status"><i class="success glyphicon glyphicon-ok"></i> ' . \Yii::t('category', 'Value.is_show.true') . '</span>';
                case false:
                    return '<span class="label label-default"> ' . \Yii::t('category', 'Value.is_show.false') . '</span>';
                default:
                    break;
            }
            return '';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            0 => \Yii::t('category', 'Filter.is_show.false'),
            1 => \Yii::t('category', 'Filter.is_show.true')
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'width' => '150px'],
        ],
        'filterInputOptions' => ['placeholder' => '---'],
    ],
    [
        'attribute' => 'is_default',
        'format' => 'raw',
        'content' => function ($data) {
            switch ($data->is_show) {
                case true:
                    return '<span class="user-status"><i class="success glyphicon glyphicon-ok"></i> ' . \Yii::t('category', 'Value.is_show.true') . '</span>';
                case false:
                    return '<span class="label label-default"> ' . \Yii::t('category', 'Value.is_show.false') . '</span>';
                default:
                    break;
            }
            return '';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            0 => \Yii::t('category', 'Filter.is_show.false'),
            1 => \Yii::t('category', 'Filter.is_show.true')
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true, 'width' => '150px'],
        ],
        'filterInputOptions' => ['placeholder' => '---'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{view} {update}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<span class="icon fa fa-eye"></span> ', $url, [
                    'class' => 'btn btn-sm btn-primary'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<span class="icon fa fa-edit"></span> ', $url, [
                    'class' => 'btn btn-sm btn-primary'
                ]);
            },
        ],
        'options' => ['style' => 'width:130px;']
    ],
];

$dynaGridOptions = [
    'columns' => $columns,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => [
            'before' => Html::a('<i class="btn-label glyphicon fa fa-plus"></i> &nbsp&nbsp' . \Yii::t('category', 'Button.add_category'), ['create'], [
                'class' => 'btn btn-labeled btn-success no-margin-t'
            ]),
            'after' => '<div class="pull-right">{pager}</div>',
        ],
        'options' => ['id' => 'grid'],
    ],
    'options' => ['id' => 'dynagrid-category'],
];

if (class_exists('\quartz\adminlteTheme\config\AnminLteThemeConfig')) {
    DynaGrid::begin(\yii\helpers\ArrayHelper::merge(\quartz\adminlteTheme\config\AnminLteThemeConfig::getDefaultConfigDynagrid(), $dynaGridOptions));
} else {
    DynaGrid::begin($dynaGridOptions);
}
DynaGrid::end();
?>

