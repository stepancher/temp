<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\category\Category */

$this->title = Yii::t('category', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('category', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
