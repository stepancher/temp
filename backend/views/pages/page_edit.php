<?php

$this->title = $model->isNewRecord ? "Создание страницы" : "Редактирование страницы";
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['update', 'id' => $model->id]];

\quartz\pages\assets\PagesAsset::register($this);
?>

<?php if (Yii::$app->session->hasFlash('message')): ?>
    <?=
    \yii\bootstrap\Alert::widget([
        'body' => Yii::$app->session->getFlash('message'),
        'options' => ['class' => 'alert-success']
    ]);
    ?>
<?php endif; ?>

<?= $this->render('@vendor/quartz/yii2-pages/views/admin/_form', [
    'model' => $model,
]) ?>

<?= \quartz\rollback\widgets\RollbackWidget::widget(['model' => $model, 'preview' => '@vendor/quartz/yii2-pages/views/admin/_form']) ?>

