<?php

use quartz\rollback\widgets\RollbackWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\Blocks */

$this->title = 'Редактирование блока';
$this->params['breadcrumbs'][] = ['label' => 'Блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';

$_form = '@vendor/quartz/yii2-blocks/views/admin/_form';
?>

    <?= $this->render($_form, [
        'model' => $model,
    ]) ?>

<?= RollbackWidget::widget(['model' => $model, 'preview' => $_form]) ?>