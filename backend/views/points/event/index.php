<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

/* @var $this yii\web\View */
/* @var $searchModel common\models\points\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('point', 'Events');
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    ['class' => 'kartik\grid\SerialColumn', 'order' => DynaGrid::ORDER_FIX_LEFT],

    'name',
    'description',
    'short_description',
    'address',
    'address_notes',
    [
        'attribute' => 'id_user',
        'value' => function ($model) {
            return is_object($model->user) ? $model->user->username : 'Не найден';
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => \common\models\user\User::getAllToList(),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => '---', 'style' => ['max-width' => '200px']],
    ],
    [
        'attribute' => 'categories',
        'value' => function ($model) {
            /**@var $model \common\models\points\Event */
            return $model->getCategoriesNamesString();
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => \common\models\category\Category::getAllToList(),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => '---', 'style' => ['max-width' => '200px']],
    ],
    [
        'attribute' => 'tags',
        'value' => function ($model) {
            /**@var $model \common\models\points\Event */
            return $model->getTagsString();
        },
    ],
    [
        'attribute' => 'created_at',
        'format' => ['date', 'php:d-m-Y H:i:s'],

        'value' => function ($data) {
            return is_null($data->created_at) ? '' : $data->created_at;
        },
        'options' => ['width' => '200'],
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => [
                    'format' => 'd-m-Y',
                ],
            ],
            'pluginEvents' => [
                "show.daterangepicker" => "function(e, p) {
                    p.firstEndDate = p.endDate.clone();
                    p.oldEndDate = moment('01-01-2000',p.locale.format);
                    p.endDate = moment('01-01-2000',p.locale.format);
                }",
                "hide.daterangepicker" => "function(e, p) {
                    if(p.firstEndDate){
                        p.endDate = p.firstEndDate.clone();
                    }
                }"
            ]
        ]
    ],
    [
        'attribute' => 'updated_at',
        'value' => function ($data) {
            return is_null($data->updated_at) ? '' : $data->updated_at;
        },
        'options' => ['width' => '200'],
        'filterType' => GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => [
                    'format' => 'd-m-Y',
                ],
            ],
            'pluginEvents' => [
                "show.daterangepicker" => "function(e, p) {
                    p.firstEndDate = p.endDate.clone();
                    p.oldEndDate = moment('01-01-2000',p.locale.format);
                    p.endDate = moment('01-01-2000',p.locale.format);
                }",
                "hide.daterangepicker" => "function(e, p) {
                    if(p.firstEndDate){
                        p.endDate = p.firstEndDate.clone();
                    }
                }"
            ]
        ]
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => '{update} {view}',
        'buttons' => [
            'update' => function ($url, $model) {
                return Html::a('<span class="icon fa fa-edit"></span> ', '/admin/points/event/update?id=' . $model->getId(), [
                    'class' => 'btn btn-sm btn-primary'
                ]);
            },
            'view' => function ($url, $model) {
                return Html::a('<span class="icon fa fa-eye"></span> ', '/admin/points/event/view?id=' . $model->getId(), [
                    'class' => 'btn btn-sm btn-info'
                ]);
            },
        ],
        'options' => ['style' => 'width:130px;']
    ],
];
$dynaGridOptions = [
    'columns' => $columns,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => [
            'before' => Html::a('<i class="btn-label glyphicon fa fa-plus"></i> &nbsp&nbsp' . \Yii::t('point', 'Add'), ['create'], [
                'class' => 'btn btn-labeled btn-success no-margin-t'
            ]),
            'after' => '<div class="pull-right">{pager}</div>',
        ],
        'options' => ['id' => 'grid'],
    ],
    'options' => ['id' => 'dynagrid-common\models\points\Event'],
];
if (class_exists('\quartz\adminlteTheme\config\AnminLteThemeConfig')) {
    DynaGrid::begin(\yii\helpers\ArrayHelper::merge(\quartz\adminlteTheme\config\AnminLteThemeConfig::getDefaultConfigDynagrid(), $dynaGridOptions));
} else {
    DynaGrid::begin($dynaGridOptions);
}
DynaGrid::end();
?>