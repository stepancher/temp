<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\points\Event */

$this->title = Yii::t('point', 'View event');
$this->params['breadcrumbs'][] = ['label' => Yii::t('point', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>

<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'short_description',
                        'description',
                        'begin_date',
                        'duration',
                        'id_city',
                        'address',
                        'address_notes',
                        'is_with_child',
                        'is_free',
                        'id_org',
                        'is_approved',
                        'views',

                        [
                            'attribute' => 'id_user',
                            'value' => is_object($model->user) ? $model->user->username : ''
                        ],
                        [
                            'attribute' => 'categories',
                            'value' =>  $model->getCategoriesNamesString()
                        ],
                        [
                            'attribute' => 'tags',
                            'value' =>  $model->getTagsString()
                        ],
                        'created_at',
                        'updated_at',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="box-footer">
            <?= Html::a(Yii::t('point', 'Update 1Event'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
</div>
