<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\points\Event */
/* @var $form yii\widgets\ActiveForm */
/* @var $is_new boolean */
?>

<div class="event-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'name')->textInput(); ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'categories')->widget(Select2::classname(), [
                'data' => \common\models\category\Category::getAllToList(),
                'language' => 'ru',
                'options' => [
                    'placeholder' => 'Выберите категории мероприятия',
                ],
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'id_user')->widget(Select2::classname(), [
                'data' => \common\models\user\User::getAllToList(),
                'language' => 'ru',
                'options' => [
                    'placeholder' => 'Выберите пользователя',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'id_org')->widget(Select2::classname(), [
                'data' => \common\models\user\User::getAllToList(),
                'language' => 'ru',
                'options' => [
                    'placeholder' => 'Выберите организатора',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'short_description')->textarea(); ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'description')->textarea(); ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'address')->textInput(); ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'address_notes')->textInput(); ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'latitude')->textInput(); ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'longitude')->textInput(); ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'is_with_child')->dropDownList([0 => 'Нет', 1 => 'Да']); ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'is_free')->dropDownList([0 => 'Нет', 1 => 'Да']); ?>
        </div>
    </div>

    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'begin_date')->widget(DateTimePicker::classname(), [
                'options' => [
                    'placeholder' => 'Введите дату события ...',
                    'id' => 'operation-date-pay-' . Yii::$app->security->generateRandomString(6)
                ],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd hh:ii',
                    'autoclose' => true,
                    'todayBtn' => true,
                    'showMeridian' => true,
                    'startView' => 4,
                    'minView' => 0
                ]
            ]); ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'id_city')->textInput(['placeholder'=>'Не доделано']); ?>
        </div>
    </div>

    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'duration')->textInput(); ?>
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'is_approved')->dropDownList([0 => 'Нет', 1 => 'Да']); ?>
        </div>
    </div>


    <div class='row'>
        <div class='col-sm-12'>
            <?= $form->field($model, 'tags')->widget(Select2::classname(), [
                'language' => 'ru',
                'options' => [
                    'placeholder' => 'Введите теги',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                    'tags' => true,
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($is_new ? Yii::t('point', 'Create') : Yii::t('point', 'Update'), ['class' => $is_new ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>