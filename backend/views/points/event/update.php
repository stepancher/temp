<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\points\Event */

$this->title = Yii::t('point', 'Update Event') . ' ' . $model->_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('point', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('point', 'Update');
?>
<div class="event-update">
    <div class="box">
        <div class="box-body">
            <?= $this->render('_form', [
                'model' => $model,
                'is_new' => false
            ]) ?>
        </div>
    </div>

</div>
