<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Закрыть/Открыть сайт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
<?php $form = ActiveForm::begin(['id' => 'plug-form']); ?>
<?= $form->field($model, 'plug_status')->checkbox() ?>
<?= $form->field($model, 'plug_interval') ?>
<?= $form->field($model, 'plug_end') ?>
<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'plug-button']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>
