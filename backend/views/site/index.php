<?php
/* @var $this yii\web\View */


$this->title = 'Админка. Типа крутые графики сводные';
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-6">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Мероприятия</span>
                    <span class="info-box-number">98 200</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                  <span class="progress-description">
                    50% увеличение за 30 дней
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-6">

            <!-- /.info-box -->
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Сообщества</span>
                    <span class="info-box-number">5,050</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 20%"></div>
                    </div>
                  <span class="progress-description">
                    20% увеличение за 30 дней
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">

            <!-- /.info-box -->
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Места</span>
                    <span class="info-box-number">18,381</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                  <span class="progress-description">
                    70% увеличение за 30 дней
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>

        <div class="col-md-6">

            <!-- /.info-box -->
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Зарегистрировано пользователей</span>
                    <span class="info-box-number">163,921</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 89%"></div>
                    </div>
                  <span class="progress-description">
                    89% из них по реферальной программе
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </div>
</div>



