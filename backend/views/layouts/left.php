<?php
use quartz\adminlteTheme\widgets\MenuNav;
use quartz\adminlteTheme\widgets\FavoritePageNav;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@quartz/adminlteTheme/assets');

?>

<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?= FavoritePageNav::widget()?>
            <?= MenuNav::widget()?>
        </ul>

        <hr/>

        <a class="logo-developer" href="http://freematiq.com" target="_blank"><img src="<?=$directoryAsset.'/img/logo-freematiq_gs.svg'?>"></a>
        <a class="logo-developer small" href="http://freematiq.com" target="_blank"><img src="<?=$directoryAsset.'/img/logo-freematiq_small_gs.svg'?>"></a>
    </section>
</aside>
