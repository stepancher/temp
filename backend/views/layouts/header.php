<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use quartz\adminlteTheme\widgets\Notifications;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
/** @var \common\models\User $user */
$user = Yii::$app->user->identity;
if (Yii::$app->hasModule('settings')) {
    $img_link = Yii::$app->getModule('settings')->model('settings')->getValueByCode('site_logo');
    $logo = !empty($img_link)? Html::img($img_link):'';
} else {
    $logo = '';
    $siteName = Yii::$app->name;
}
?>
<header class="main-header">

    <?= Html::a($logo, Yii::$app->homeUrl . '/../', ['class' => 'logo', 'target' => '_blank']) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?=Notifications::widget()?>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <?php
                        if (isset($user->profile)) {
                            echo Html::img($user->profile->getProfileAvatar('25x25'), ['class' => 'user-image', 'alt' => $user->username]);
                        }
                        ?>
                        <span class="hidden-xs"><?= $user->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">

                            <?php
                            if (isset($user->profile)) {
                                echo Html::img($user->profile->getProfileAvatar('90x90'), ['class' => 'img-circle', 'alt' => $user->username]);
                            }
                            ?>
                            <p>
                                <?= $user->username ?>
                                <small><?= Yii::t('adminlte', 'Header.registered') ?>:
                                    <b><?= Yii::$app->formatter->asDateTime($user->created_at, Yii::$app->formatter->dateFormat) ?></b>
                                    <small><?= Yii::$app->formatter->asDateTime($user->created_at, Yii::$app->formatter->timeFormat) ?></small>
                                </small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <?= Html::a(
                                    Yii::t('adminlte', 'Header.logout'),
                                    ['/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
<!--                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
        <?php if(Yii::$app->getModule('adminlteTheme')->checkFavorite) { ?>
        <a data-toggle="modal" data-target="#favorite-modal"><i class="favorite-fa fa fa-star-half-o fa-2x" aria-hidden="true"></i></a>
        <?php }?>
    </nav>

</header>


<?php
if(Yii::$app->getModule('adminlteTheme')->checkFavorite) {
    echo $this->render(Yii::$app->getModule('adminlteTheme')->view('add_favorite'));
}
?>