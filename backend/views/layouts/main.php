<?php
use yii\helpers\Html;


/* @var $this \yii\web\View */
/* @var $content string */

if (Yii::$app->controller->action->id === 'login') {
    echo $this->render(
        'wrapper-black',
        ['content' => $content]
    );
} else {
    \quartz\adminlteTheme\assets\AdminLteFixAsset::register($this);
    ?>

    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="" type="text/javascript"></script>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="sidebar-mini <?= \quartz\adminlteTheme\helpers\AdminLteHelper::skinClass() ?>">

    <?php $this->beginBody() ?>
    <?= $this->render(
        'header.php'
    ) ?>

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <?= $this->render(
            'left.php'
        )
        ?>
        <?= $this->render(
            'content.php',
            ['content' => $content]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
    <?php
}
?>
