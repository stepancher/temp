<?php

use yii\db\Migration;

class m160428_030331_add_settings extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%settings}}', ['name', 'code', 'value', 'type'], [
            ['Секретный ключ ReCaptcha', 're_captcha_secret', '', 'string'],
            ['Ключ ReCaptcha', 're_captcha_site_key', '', 'string'],

        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%settings}}', ['code' => 're_captcha_secret']);
        $this->delete('{{%settings}}', ['code' => 're_captcha_site_key']);
    }
}
