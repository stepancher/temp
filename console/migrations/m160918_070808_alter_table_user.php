<?php

use yii\db\Migration;

class m160918_070808_alter_table_user extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'id_referer', $this->integer());
        $this->addColumn('{{%user}}', 'code', $this->string()->unique());
        $this->update('{{%user}}', ['code'=> 3244200611], ['id' => 1]);
        $this->addColumn('{{%user}}', 'id_city', $this->bigInteger());
        $this->addColumn('{{%user}}', 'id_language', $this->integer());

        $this->dropColumn('{{%user_profile}}', 'icq');

        $this->addColumn('{{%user_profile}}', 'site', $this->string());
        $this->addColumn('{{%user_profile}}', 'about_me', $this->string());
        $this->addColumn('{{%user_profile}}', 'is_allergy', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%user_profile}}', 'is_disability', $this->boolean()->defaultValue(false));

        $this->dropColumn('{{%user_profile}}', 'sex');
        $this->addColumn('{{%user_profile}}', 'sex', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'id_referer');
        $this->dropColumn('{{%user}}', 'code');
        $this->dropColumn('{{%user}}', 'id_city');
        $this->dropColumn('{{%user}}', 'id_language');

        $this->addColumn('{{%user_profile}}', 'icq', $this->string(100));

        $this->dropColumn('{{%user_profile}}', 'site');
        $this->dropColumn('{{%user_profile}}', 'about_me');
        $this->dropColumn('{{%user_profile}}', 'is_allergy');
        $this->dropColumn('{{%user_profile}}', 'is_disability');

    }
}
