<?php

use yii\db\Migration;

class m161105_114222_add_permissions_category extends Migration
{
    private $permissionsAdmin = [
        'r_category' => 'Просмотр информации о категориях',
        'w_category' => 'Редактирование информации о категориях',
    ];

    public function safeUp()
    {
        //add roles and permissions
        $roleModer = Yii::$app->authManager->getRole('moderator');
        $roleAdmin = Yii::$app->authManager->getRole('admin');
        $roleDev = Yii::$app->authManager->getRole('developer');


        foreach($this->permissionsAdmin as $name => $desc) {
            $perm = Yii::$app->authManager->createPermission($name);
            $perm->description = $desc;
            Yii::$app->authManager->add($perm);

            if($roleAdmin) Yii::$app->authManager->addChild($roleAdmin, $perm);
            if($roleModer) Yii::$app->authManager->addChild($roleModer, $perm);
            if($roleDev) Yii::$app->authManager->addChild($roleDev, $perm);
        }
        \yii\caching\TagDependency::invalidate(Yii::$app->cache, 'rbac');
    }

    public function safeDown()
    {
        foreach($this->permissionsAdmin as $name => $desc) {
            $perm = Yii::$app->authManager->getPermission($name);
            if ($perm) {
                Yii::$app->authManager->remove($perm);
            }
        }
        \yii\caching\TagDependency::invalidate(Yii::$app->cache, 'rbac');

    }
}
