<?php

use yii\db\Migration;

class m161105_112417_alter_category extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'is_default', $this->boolean()->defaultValue(false)->notNull());
        $this->update('{{%category}}', ['is_default' => true], ['id' => [1, 3, 6, 10, 14]]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'is_default');
    }
}
