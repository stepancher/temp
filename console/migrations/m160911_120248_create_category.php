<?php

use yii\db\Migration;

class m160911_120248_create_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'is_show' => $this->boolean()->defaultValue(true),
            'created_at' => 'timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%category}}');
    }
}
