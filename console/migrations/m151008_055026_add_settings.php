<?php

use yii\db\Migration;

class m151008_055026_add_settings extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%settings}}', ['name', 'code', 'value', 'type'], [
            ['Название сайта', 'site_name', 'Tetrus.club', 'string'],
            ['Логотип сайта', 'site_logo', '', 'image'],
            ['Email администратора', 'admin_email', 'admin@tetrus.club', 'string'],

        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%settings}}', ['code' => 'site_name']);
        $this->delete('{{%settings}}', ['code' => 'site_logo']);
        $this->delete('{{%settings}}', ['code' => 'admin_email']);
    }
}
