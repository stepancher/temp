<?php

use yii\db\Migration;

class m160911_161204_add_categories extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%category}}', ['name', 'created_at', 'updated_at'], [
            ['Неформальные', 'NOW()', 'NOW()'],
            ['Знакомства', 'NOW()', 'NOW()'],
            ['Конференции и форумы', 'NOW()', 'NOW()'],
            ['Обучающее', 'NOW()', 'NOW()'],
            ['Дни открытых дверей', 'NOW()', 'NOW()'],
            ['Турниры и поединки', 'NOW()', 'NOW()'],
            ['Гонки и марафоны', 'NOW()', 'NOW()'],
            ['Игры и квесты', 'NOW()', 'NOW()'],
            ['Тренировки и тренинги', 'NOW()', 'NOW()'],
            ['Активный отдых', 'NOW()', 'NOW()'],
            ['Праздники и ярмарки', 'NOW()', 'NOW()'],
            ['Концерты и шоу', 'NOW()', 'NOW()'],
            ['Фестивали', 'NOW()', 'NOW()'],
            ['Праздники и вечеринки', 'NOW()', 'NOW()'],
            ['Выставки и показы', 'NOW()', 'NOW()'],
            ['Публичные акции', 'NOW()', 'NOW()'],
        ]);

    }

    public function safeDown()
    {
        $this->truncateTable('{{%category}}');
    }

}
