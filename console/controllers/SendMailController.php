<?php

namespace console\controllers;

use yii\console\Controller;
use vendor\quartz\mailnotify\models\MailNotifyStore;

/**
 * Консольный скрипт отправки почты из базы писем
 *
 * Class SendMailController
 * @package console\controllers
 */
class SendMailController extends Controller
{
    public function actionIndex()
    {

        $mails = MailNotifyStore::find()
            ->where(['processed' => false])
            ->limit(10)
            ->all();

        if($mails) {
            // Отправка писем
            /** @var MailNotifyStore $mail */
            foreach ($mails as $mail) {
                if(\Yii::$app->mailer->compose()
                    ->setFrom($mail->address_from)
                    ->setTo($mail->address_to)
                    ->setSubject($mail->title)
                    ->setHtmlBody($mail->message)
                    ->send()) {

                    // echo 'Mail from: '.$mail->address_from.' to: '.$mail->address_to.' was successfully sent'.PHP_EOL;
                    $mail->processed = true;
                    $mail->save();
                } else {
                    // TODO: продумать, если письмо не отправилось
                    continue;
                }
            }
        }
    }

}
