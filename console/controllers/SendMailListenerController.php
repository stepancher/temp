<?php

namespace console\controllers;

use vendor\quartz\mailnotify\models\SendMailListenerWorker;
use yii\console\Controller;

/**
 * Демон для отправки писем
 *
 * Class SendMailListenerController
 * @package console\controllers
 */
class SendMailListenerController extends Controller
{
    // Отправка писем
    public function actionIndex()
    {
        $worker = new SendMailListenerWorker();
        $worker->run();
    }
}
