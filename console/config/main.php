<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
  );

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'localization'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
    'modules' => [
        'localization' => [
            'class' => 'quartz\localization\Localization',
            'messagesLocations' => [
                '@root/common/messages',
                '@root/vendor/quartz/yii2-rbac/messages',
                '@root/vendor/quartz/yii2-servertest/messages',
                '@root/vendor/quartz/yii2-adminlte-theme/messages',
                '@root/vendor/quartz/yii2-tools/messages'
            ]
        ],
    ],
];
